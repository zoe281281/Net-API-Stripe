SYNOPSIS
========

        my $stripe = Net::API::Stripe->new({
            debug => 3,
            conf_file => './stripe-settings.json',
            livemode => 0,
            ignore_unknown_parameters => 1,
            expand => 'all',
        }) || die( Net::API::Stripe->error );

A Stripe json settings file looks like this:

        {
            "livemode": false,
            "test_secret_key": "sk_test_1234567890abcdefg",
            "test_public_key": "pk_test_1234567890abcdefg",
            "live_secret_key": "sk_live_0987654321zyxwvut",
            "live_public_key": "pk_live_0987654321zyxwvut",
            "version": "2020-03-02",
        }

Create a customer:

        # Create an address object
        my $addr;
        if( $v->{street} || $v->{city} || $v->{postal_code} || $v->{country} )
        {
            $addr = $stripe->address({
                line1 => $v->{street},
                line2 => $v->{building},
                city => $v->{city},
                postal_code => $v->{postal_code},
                state => $v->{state},
                country => $v->{country},
            }) || bailout( "Unable to create a postal address object: ", $stripe->error );
        }
        my $cust_object = $stripe->customer({
            balance => 20000,
            address => $addr,
            # Must be set up previously before using it
            coupon => '2020DISCOUNT50',
            # Japanese Yen
            currency => 'jpy',
            description => 'VIP customer',
            email => 'john@example.com',
            invoice_prefix => 'VIP',
            # Default payment must be set up beforehand for it to be declared here
            invoice_settings => { default_payment_method => 'pm_fake1234567' },
            metadata => { db_id => 123, process_id => 456 },
            name => 'John Doe',
            phone => '+81-90-1234-5678',
            preferred_locales => [qw( en ja )],
            shipping => $addr,
        });

        # Submit this customer to Stripe for creation
        my $cust = $stripe->customers( create => $cust_object ) || die( sprintf( "Failed with error message %s and code %d\n", $stripe->error->message, $stripe->error->code ) );

Retrieve customer:

        my $cust = $stripe->customers( retrieve => 'cust_fake12345' );
        # or we can also pass a customer object
        my $cust = $stripe->customers( retrieve => $cust_object ) || do
        {
            if( $stripe->http_response->code == 404 )
            {
                die( "Customer ", $cust_object->id, " does not exist!\n" );
            }
            else
            {
                die( "Some unexpected error occurred: ", $stripe->error, "\n" );
            }
        };

Other methods are describe below and the parameters they take are
documented in their respective module.

VERSION
=======

        v1.1.0

DESCRIPTION
===========

This is a comprehensive Stripe API. It provides an object oriented
friendly interface for which I put a lot of hard work so you could spend
your time on other aspects of your development.

It inherits from
[Module::Generic](https://metacpan.org/pod/Module::Generic){.perl-module}
and
[Net::API::Stripe](https://metacpan.org/pod/Net::API::Stripe){.perl-module}
sub modules inherits from
[Net::API::Stripe::Generic](https://metacpan.org/pod/Net::API::Stripe::Generic){.perl-module}

This interface aims at making it easy to make api calls to Stripe,
however it is important and a time-saver to read Stripe documentation
comprehensively.

This interface will do minimal data integrity check. Thus, even though
this interface will check for proper data types like array, right
property names used, mandatory parameters in api calls, etc it does not
do any check on the data itself, so you should always check for return
value from Stripe api calls and look at the Stripe error returned. If an
error occured, the Stripe api method will return undef and set an error
message accordingly. See [\"ERROR
HANDLING\"](#error-handling){.perl-module}

CONSTRUCTOR
===========

new( %ARG )
-----------

Creates a new
[Net::API::Stripe](https://metacpan.org/pod/Net::API::Stripe){.perl-module}
objects. It may also take an hash like arguments, that also are method
of the same name.

*api\_uri*

:   The base uri of the Stripe API. This should not be changed.

*browser*

:   The user agent id to use when making http api calls

*conf\_file*

:   The file path to the configuration file. Each property in this
    configuration file is same as the parameters to instantiate a new
    [Net::API::Stripe](https://metacpan.org/pod/Net::API::Stripe){.perl-module}
    object.

*debug*

:   Toggles debug mode on/off

*expand*

:   Integer. Sets the depth level of expansion of Stripe objects. If
    objects are not expanded, Stripe API will return the object id, but
    when they are expanded, Stripe returns the entire object and its
    properties. You can then chain your code and do something like:

            print $cust->invoice_settings->default_payment_method->type

*ignore\_unknown\_parameters*

:   Boolean. When this is on, this will ignore any properties sent back
    from Stripe API that are unknown to us. This happens frequently as
    Stripe updates its API. When this value is set to false, then
    unknown properties will cause this to stop processing and return an
    error.

*livemode*

:   Boolean value to toggle test or live mode

*verbose*

:   Toggles verbose mode on/off

*version*

:   The version of the Stripe API to use. Example `2020-03-02`

METHODS
=======

account
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::Account](https://metacpan.org/pod/Net::API::Stripe::Connect::Account){.perl-module}
object.

account\_link
-------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::Account::Link](https://metacpan.org/pod/Net::API::Stripe::Connect::Account::Link){.perl-module}
object.

address
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Address](https://metacpan.org/pod/Net::API::Stripe::Address){.perl-module}
object.

amount
------

Provided with a number, this returns a
[Module::Generic::Number](https://metacpan.org/pod/Module::Generic::Number){.perl-module}
object, which extends
[Number::Format](https://metacpan.org/pod/Number::Format){.perl-module}

api\_uri
--------

Returns the [URI](https://metacpan.org/pod/URI){.perl-module} object of
the Stripe api.

application\_fee
----------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::ApplicationFee](https://metacpan.org/pod/Net::API::Stripe::Connect::ApplicationFee){.perl-module}
object.

application\_fee\_refund
------------------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::ApplicationFee::Refund](https://metacpan.org/pod/Net::API::Stripe::Connect::ApplicationFee::Refund){.perl-module}
object.

authorization
-------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Issuing::Authorization](https://metacpan.org/pod/Net::API::Stripe::Issuing::Authorization){.perl-module}
object.

balance
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Balance](https://metacpan.org/pod/Net::API::Stripe::Balance){.perl-module}
object.

balance\_transaction
--------------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Balance::Transaction](https://metacpan.org/pod/Net::API::Stripe::Balance::Transaction){.perl-module}
object.

bank\_account
-------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}
object.

browser
-------

Set or get the user agent string used when making calls to Stripe API.

capability
----------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::Account::Capability](https://metacpan.org/pod/Net::API::Stripe::Connect::Account::Capability){.perl-module}
object.

card\_holder
------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Issuing::Card::Holder](https://metacpan.org/pod/Net::API::Stripe::Issuing::Card::Holder){.perl-module}
object.

card
----

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::ExternalAccount::Card](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Card){.perl-module}
object.

charge
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}
object.

code2error
----------

Given a code returned by Stripe upon error, this returns the
corresponding string.

        my $cust = $stripe->customers( retrieve => $id ) || 
          die( $stripe->code2error( $stripe->error->code ), "\n" );

conf\_file( \[ file path \] )
-----------------------------

Given a json configuration file, it will read the data, set the property
*conf\_data* to the decoded hash and return it. When called without
argument, it returns the current value of *conf\_data*.

connection\_token
-----------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Terminal::ConnectionToken](https://metacpan.org/pod/Net::API::Stripe::Terminal::ConnectionToken){.perl-module}
object.

country\_spec
-------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::CountrySpec](https://metacpan.org/pod/Net::API::Stripe::Connect::CountrySpec){.perl-module}
object.

coupon
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}
object.

credit\_note
------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object.

currency
--------

Set or get the 3-letter iso 4217 currency, such as `jpy` for Japanese
yen or `eur` for Euro.

customer
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object.

customer\_balance\_transaction
------------------------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Customer::BalanceTransaction](https://metacpan.org/pod/Net::API::Stripe::Customer::BalanceTransaction){.perl-module}
object.

customer\_tax\_id
-----------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Customer::TaxId](https://metacpan.org/pod/Net::API::Stripe::Customer::TaxId){.perl-module}
object.

delete( END POINT, HASH PAYLOAD )
---------------------------------

Given a Stripe end point as a URI absolute path, and a payload as a hash
reference, this will issue a `DELETE` http query and return a hash
reference corresponding to the json data returned by Stripe, or, in case
of error, it will return undef and set the error which can be accessed
with `$stripe-`error\> (a
[Module::Generic::Exception](https://metacpan.org/pod/Module::Generic::Exception){.perl-module}
object).

discount
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Discount](https://metacpan.org/pod/Net::API::Stripe::Billing::Discount){.perl-module}
object.

dispute
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Dispute](https://metacpan.org/pod/Net::API::Stripe::Dispute){.perl-module}
object.

encode\_with\_json
------------------

Takes a bollean value. This is used to set whether the payload should be
encoded with json. This should not be changed.

event
-----

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Event](https://metacpan.org/pod/Net::API::Stripe::Event){.perl-module}
object.

expand
------

Integer. Sets or get the depth of Stripe object expansion. See Stripe
api documentation for more information:
<https://stripe.com/docs/api/expanding_objects>

fields
------

Given an object type, this returns an array reference of all the methods
(aka fields) for that module.

file
----

Provided with optional hash parameters, this returns a
[Net::API::Stripe::File](https://metacpan.org/pod/Net::API::Stripe::File){.perl-module}
object.

file\_link
----------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::File::Link](https://metacpan.org/pod/Net::API::Stripe::File::Link){.perl-module}
object.

fraud
-----

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Fraud](https://metacpan.org/pod/Net::API::Stripe::Fraud){.perl-module}
object.

generate\_uuid
--------------

Returns a uuid version 4. This uses
[Data::UUID](https://metacpan.org/pod/Data::UUID){.perl-module} to
achieve that.

get( END POINT, HASH PAYLOAD )
------------------------------

Given a Stripe absolute uri and a hash reference, this will issue a http
`GET` request and return a hash reference representing the json data
returned by Stripe or undef if an error occurred. The error can then be
retrieved like `$stripe-`error\> which is a
[Module::Generic::Exception](https://metacpan.org/pod/Module::Generic::Exception){.perl-module}
object.

http\_client
------------

This returns the
[LWP::UserAgent](https://metacpan.org/pod/LWP::UserAgent){.perl-module}
object and create it if it is not yet instantiated.

http\_request
-------------

Get or set the
[HTTP::Request](https://metacpan.org/pod/HTTP::Request){.perl-module}
based on the data provided.

http\_response
--------------

Get or set the
[HTTP::Response](https://metacpan.org/pod/HTTP::Response){.perl-module}
based on the data provided.

ignore\_unknown\_parameters
---------------------------

Boolean. When true, this module will ignore unknown properties returned
from calls made to Stripe api. if set to false, and an unknown property
is received, this will generate an error and return undef, stopping the
flow of the request instead of ignoring it.

invoice
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

invoice\_item
-------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Invoice::Item](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice::Item){.perl-module}
object.

invoice\_line\_item
-------------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Invoice::LineItem](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice::LineItem){.perl-module}
object.

invoice\_settings
-----------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Invoice::Settings](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice::Settings){.perl-module}
object.

issuing\_card
-------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Issuing::Card](https://metacpan.org/pod/Net::API::Stripe::Issuing::Card){.perl-module}
object.

issuing\_dispute
----------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Issuing::Dispute](https://metacpan.org/pod/Net::API::Stripe::Issuing::Dispute){.perl-module}
object.

issuing\_transaction
--------------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Issuing::Transaction](https://metacpan.org/pod/Net::API::Stripe::Issuing::Transaction){.perl-module}
object.

json
----

This returns a `JSON` object with option *allow\_nonref* enabled.

key( STRIPE API SECRET KEY )
----------------------------

Provided with your Stripe api secret key, this will set this property
accordingly, but will also set the **auth** property as well. **auth**
is used to authenticate you when making calls to Stripe api. **auth**
would be something like this:

        Basic c2tfMTIzNDU2Nzg5MGFiY2RlZmdoaWo6Cg

livemode
--------

Boolean. Set or get the livemode status. If it is true, then all api
query will be mad in live mode.

location
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Terminal::Location](https://metacpan.org/pod/Net::API::Stripe::Terminal::Location){.perl-module}
object.

login\_link
-----------

Provided with optional hash parameters this returns a
[Net::API::Stripe::Connect::Account::LoginLink](https://metacpan.org/pod/Net::API::Stripe::Connect::Account::LoginLink){.perl-module}
object.

order
-----

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Order](https://metacpan.org/pod/Net::API::Stripe::Order){.perl-module}
object.

order\_item
-----------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Order::Item](https://metacpan.org/pod/Net::API::Stripe::Order::Item){.perl-module}
object.

payment\_intent
---------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object.

payment\_method
---------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object.

payout
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Payout](https://metacpan.org/pod/Net::API::Stripe::Payout){.perl-module}
object.

person
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::Person](https://metacpan.org/pod/Net::API::Stripe::Connect::Person){.perl-module}
object.

plan
----

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object.

post( END POINT, HASH PAYLOAD )
-------------------------------

Given a Stripe end point absolute uri and a hash reference, this will
issue a `POST` http request to the Stripe api and return a hash
reference representing the object provided by Stripe or undef with an
error set, which can be retrieved using the
[\"error\"](#error){.perl-module} method.

If no *idempotency* parameter was provided, **post** will automatically
create one.

post\_multipart( END POINT, HASH PAYLOAD )
------------------------------------------

Given a Stripe end point absolute uri and a hash reference, this will
issue a `POST` multipart http request to the Stripe api and return a
hash reference representing the object returned by Stripe. If an error
had occurred, it will return undef and set an error that can be
retrieved using the [\"error\"](#error){.perl-module} method.

This method is used primarily when upload file. See the section below on
[\"FILES\"](#files){.perl-module}

product
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Product](https://metacpan.org/pod/Net::API::Stripe::Product){.perl-module}
object.

reader
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Terminal::Reader](https://metacpan.org/pod/Net::API::Stripe::Terminal::Reader){.perl-module}
object.

refund
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Refund](https://metacpan.org/pod/Net::API::Stripe::Refund){.perl-module}
object.

return
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Order::Return](https://metacpan.org/pod/Net::API::Stripe::Order::Return){.perl-module}
object.

review
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Fraud::Review](https://metacpan.org/pod/Net::API::Stripe::Fraud::Review){.perl-module}
object.

schedule
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object.

session
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Session](https://metacpan.org/pod/Net::API::Stripe::Session){.perl-module}
object.

schedule\_query
---------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Sigma::ScheduledQueryRun](https://metacpan.org/pod/Net::API::Stripe::Sigma::ScheduledQueryRun){.perl-module}
object.

session
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Checkout::Session](https://metacpan.org/pod/Net::API::Stripe::Checkout::Session){.perl-module}
object.

setup\_intent
-------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Payment::Intent::Setup](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent::Setup){.perl-module}
object.

shipping
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Shipping](https://metacpan.org/pod/Net::API::Stripe::Shipping){.perl-module}
object.

sku
---

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Order::SKU](https://metacpan.org/pod/Net::API::Stripe::Order::SKU){.perl-module}
object.

source
------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object.

subscription
------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object.

subscription\_item
------------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::Subscription::Item](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Item){.perl-module}
object.

tax\_ids
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::TaxID](https://metacpan.org/pod/Net::API::Stripe::Billing::TaxID){.perl-module}
object.

tax\_rate
---------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Tax::Rate](https://metacpan.org/pod/Net::API::Stripe::Tax::Rate){.perl-module}
object.

token
-----

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Token](https://metacpan.org/pod/Net::API::Stripe::Token){.perl-module}
object.

topup
-----

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::TopUp](https://metacpan.org/pod/Net::API::Stripe::Connect::TopUp){.perl-module}
object.

transfer
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::Transfer](https://metacpan.org/pod/Net::API::Stripe::Connect::Transfer){.perl-module}
object.

transfer\_reversal
------------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::Transfer::Reversal](https://metacpan.org/pod/Net::API::Stripe::Connect::Transfer::Reversal){.perl-module}
object.

usage\_record
-------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Billing::UsageRecord](https://metacpan.org/pod/Net::API::Stripe::Billing::UsageRecord){.perl-module}
object.

value\_list
-----------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Fraud::ValueList](https://metacpan.org/pod/Net::API::Stripe::Fraud::ValueList){.perl-module}
object.

value\_list\_item
-----------------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Fraud::ValueList::Item](https://metacpan.org/pod/Net::API::Stripe::Fraud::ValueList::Item){.perl-module}
object.

version
-------

Set or get the api version. This must be set on the [Stripe
dashboard](https://dashboard.stripe.com/){.perl-module}

webhook
-------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::WebHook::Object](https://metacpan.org/pod/Net::API::Stripe::WebHook::Object){.perl-module}
object.

transfer
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::Transfer](https://metacpan.org/pod/Net::API::Stripe::Connect::Transfer){.perl-module}
object.

transfer
--------

Provided with optional hash parameters, this returns a
[Net::API::Stripe::Connect::Transfer](https://metacpan.org/pod/Net::API::Stripe::Connect::Transfer){.perl-module}
object.

BALANCE TRANSACTION
-------------------

You can **retrieve** or **list** the balance transactions.

### list

This can take various parameter to influence the list of data returned
by Stripe. It returns a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object of
[Net::API::Stripe::Balance::Transaction](https://metacpan.org/pod/Net::API::Stripe::Balance::Transaction){.perl-module}
objects. Valid parameters are as follows. See Stripe API for more
information: <https://stripe.com/docs/api/balance_transactions/list>

        my $list = $stripe->balance_transactions( 'list' ) || die( $stripe->error );
        while( my $bt = $list->next )
        {
            printf( <<EOT, $bt->id, $bt->amount, $bt->created->iso8601, $bt->currency, $bt->customer->name, $bt->description );
    Id: %s
    Amount: %s
    Created on: $s
    Currency: %s
    Cusomer name: %s
    Description: %s
    EOT
        }

Possible parameters are:

*available\_on*

:   

*created*

:   

*currency*

:   3-letter iso 4217 currency

*ending\_before*

:   Stripe balance transaction id

*limit*

:   Integer

*payout*

:   

*source*

:   

*starting\_after*

:   

*type*

:   Only returns transactions of the given type

### retrieve

        my $trans = $stripe->balances( retrieve => 'txn_fake1234567890' ) || die( $stripe->error );

Provided a `balance_transaction` object or an id, this returns a
[Net::API::Stripe::Balance::Transaction](https://metacpan.org/pod/Net::API::Stripe::Balance::Transaction){.perl-module}
object or undef upon error.

BANK ACCOUNT
------------

You can **create**, **retrieve**, **update**, **delete** or **list**
bank acounts.

### create

        my $acct = $stripe->bank_accounts( create => $stripe->bank_account({
            account_holder_name => 'Big Corp, Inc',
            account_holder_type => 'company',
            bank_name => 'Big Bank, Corp'
            country => 'us',
            currency => 'usd',
            # Net::API::Stripe::Customer object
            customer => $customer_object,
            default_for_currency => $stripe->true,
            fingerprint => 'kshfkjhfkjsjdla',
            last4 => 1234,
            metadata => { transaction_id => 2222 },
            routing_number => 123,
            status => 'new',
        })) || die( "Oops: ", $stripe->error );

Provided wuth a bank account object
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}
that has its *account* property set, or simply a hash reference this
will create a Stripe bank account and return its object as a
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}

Possible parameters are:

*account*

:   A Stripe account id. This is required.

*external\_account* This is required. Either a token, like the ones returned by Stripe.js, or a hash reference containing a user's bank account details with the following properties:

:   

    *object* (required)

    :   

    *country* (required)

    :   

    *currency* (required)

    :   

    *account\_holder\_name*

    :   

    *account\_holder\_type*

    :   

    *routing\_number*

    :   

    *account\_number* (required)

    :   

*default\_for\_currency* Boolean

:   

*metadata*

:   An arbitrary hash reference

For more information see Stripe documentation here:
<https://stripe.com/docs/api/external_account_bank_accounts/create>

### retrieve

        my $acct = $stripe->bank_accounts( retrieve => 'ba_fake123456789' ) || die( "Oops: ", $stripe->error );

Provided wuth a bank account object
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}
that has its *account* property set, or simply a hash reference this
will retrieve a Stripe bank account and return its object as a
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}

Possible parameters are:

*id*

:   A Stripe bank account id. This is required.

*account*

:   A Stripe account id. This is required.

For more information see Stripe documentation here:
<https://stripe.com/docs/api/external_account_bank_accounts/retrieve>

### update

        my $acct = $stripe->bank_accounts( update => $stripe->bank_account({
            account_holder_name => 'Big Corp, Co., Ltd.',
            default_for_currency => $stripe->false,
        })) || die( "Oops: ", $stripe->error );

        # or passing a hash rather than an object

        my $acct = $stripe->bank_accounts( update => {
            account_holder_name => 'Big Corp, Co., Ltd.',
            default_for_currency => $stripe->false,
        }) || die( "Oops: ", $stripe->error );

Provided wuth a bank account object
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}
that has its *account* property set, or simply a hash reference this
will update a Stripe bank account and return its object as a
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}

Possible parameters are:

*id*

:   A Stripe bank account id. This is required.

*account*

:   A Stripe account id. This is required.

*account\_holder\_name* String

:   

*account\_holder\_type* String

:   

*default\_for\_currency* Boolean

:   

*metadata*

:   An arbitrary hash reference

For more information see Stripe documentation here:
<https://stripe.com/docs/api/external_account_bank_accounts/update>

### delete

        my $removed_acct = $stripe->bank_accounts( delete => 'ba_fake123456789' ) || die( "Oops: ", $stripe->error );

Provided wuth a bank account object
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}
that has its *account* property set, or simply a hash reference this
will remove a Stripe bank account and return its object as a
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}

Possible parameters are:

*id*

:   A Stripe bank account id. This is required.

*account*

:   A Stripe account id. This is required.

For more information see Stripe documentation here:
<https://stripe.com/docs/api/external_account_bank_accounts/delete>

### list

        my $list = $stripe->bank_accounts( 'list' ) || die( $stripe->error );
        printf( "%d total transaction(s) found\n", $list->count );
        while( my $acct = $list->next )
        {
            ## Do something with this object
        }

Provided wuth a bank account object
[Net::API::Stripe::Connect::ExternalAccount::Bank](https://metacpan.org/pod/Net::API::Stripe::Connect::ExternalAccount::Bank){.perl-module}
that has its *account* property set, or simply a hash reference this
will list all Stripe bank accounts and return a list object as a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}

Possible parameters are:

*account*

:   A Stripe account id. This is required.

For more information see Stripe documentation here:
<https://stripe.com/docs/api/external_account_bank_accounts/list>

CARD
----

You can **create**, **retrieve**, **update**, **delete** or **list**
cards.

### create

Provided a customer object
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
or a card object
[Net::API::Stripe::Payment::Card](https://metacpan.org/pod/Net::API::Stripe::Payment::Card){.perl-module}
that has its *customer* property set, or simply a hash reference this
will create a Stripe card and return its object as a
[Net::API::Stripe::Payment::Card](https://metacpan.org/pod/Net::API::Stripe::Payment::Card){.perl-module}

Possible parameters are:

*id*

:   A customer id

*source*

:   A hash reference with the following properties: object number
    exp\_month exp\_year cvc currency name metadata
    default\_for\_currency address\_line1 address\_line2 address\_city
    address\_state address\_zip address\_country

*metadata* An arbitrary hash reference

:   

### retrieve

Provided a customer object
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
or a card object
[Net::API::Stripe::Payment::Card](https://metacpan.org/pod/Net::API::Stripe::Payment::Card){.perl-module}
that has its *customer* property set, or simply a hash reference this
will retrieve the customer card information as a
[Net::API::Stripe::Payment::Card](https://metacpan.org/pod/Net::API::Stripe::Payment::Card){.perl-module}
object

Possible parameters are:

*id*

:   Stripe card id

*customer*

:   Stripe customer id

### update

Provided a customer object
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
or a card object
[Net::API::Stripe::Payment::Card](https://metacpan.org/pod/Net::API::Stripe::Payment::Card){.perl-module}
that has its *customer* property set, or simply a hash reference this
will update the customer card information, but what can be updated is
limited by Stripe and it is typically the expiration date or postal
address

Possible parameters are:

*id*

:   Stripe card id

*customer*

:   Stripe customer id

*address\_city* City

:   

*address\_country*

:   Country as 2-letters ISO 3166 country code

*address\_line1*

:   Address first line

*address\_line2*

:   Address second line

*address\_state*

:   State / region

*address\_zip*

:   Postal code

*exp\_month*

:   Expiration month

*exp\_year*

:   Expiration year

*metadata*

:   Arbitrary hash reference

*name*

:   Name for this credit card

### delete

Provided with a
[customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
or a
[card](https://metacpan.org/pod/Net::API::Stripe::Payment::Card){.perl-module}
object, or a hash reference, this will issue an api call to Stripe to
remove the customer\'s card. It returns the card object that as deleted
with its property *deleted* set to true.

Possible parameters are:

*id*

:   Stripe customer id

*card\_id*

:   Stripe card id

### list

Provided with a
[customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object, this issue an api call to get the list of all cards for a given
customer.

Possible parameters are:

*id*

:   Stripe customer id

*ending\_before*

:   A card id

*limit* Integer

:   

*starting\_after* A card id

:   

For more information, see Stripe api documentation here:
<https://stripe.com/docs/api/cards/list>

CHARGE
------

You can **create**, **retrieve**, **update**, **capture** or **list**
charges.

### create

Provided with a
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}
object or a hash reference, this will create a Stripe charge and return
a charge object
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}

Possible parameters are:

*amount* Amount as integer. This is required

:   

*currency* A 3-letter iso 4217 code such sa `jpy` for Japanese Yen

:   

*application\_fee\_amount* Integer

:   

*capture* Boolean

:   

*customer* A customer id

:   

*description* An arbitrary text

:   

*destination* A hash with properties account and amount.

:   

*metadata* Arbitrary hash reference

:   

*on\_behalf\_of* Stripe account id

:   

*receipt\_email* E-mail address

:   

*shipping* Shipping address as a hash reference with the following properties: address name carrier phone tracking\_number. See also [Net::API::Stripe::Shipping](https://metacpan.org/pod/Net::API::Stripe::Shipping){.perl-module}

:   

*source* A source id

:   

*statement\_descriptor* Text

:   

*statement\_descriptor\_suffix* Text

:   

*transfer\_data* A date

:   

*transfer\_group* Text

:   

*idempotency* identifier

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/charges/create>

### retrieve

Provided with a
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}
object or a hash reference, this will retrieve a Stripe charge and
return its corresponding charge object
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}

Possible parameters are:

*id* Stripe charge id. This is required

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/charges/retrieve>

### update

Provided with a
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}
object or a hash reference, this will update a Stripe charge and return
its corresponding charge object
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}

Possible parameters are:

*id* A Stripe charge id. This is required

:   

*customer* A customer id

:   

*description* An arbitrary text

:   

*fraud\_details* A hash with one property: *user\_report*

:   

*metadata* Arbitrary hash reference

:   

*receipt\_email* E-mail address

:   

*shipping* Shipping address as a hash reference with the following properties: address name carrier phone tracking\_number. See also [Net::API::Stripe::Shipping](https://metacpan.org/pod/Net::API::Stripe::Shipping){.perl-module}

:   

*transfer\_group* Text

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/charges/update>

### capture

Provided with a
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}
object or a hash reference, this will capture a Stripe charge and return
its corresponding charge object
[Net::API::Stripe::Charge](https://metacpan.org/pod/Net::API::Stripe::Charge){.perl-module}

Possible parameters are:

*id* A Stripe charge id. This is required

:   

*amount* Integer

:   

*application\_fee\_amount* Integer

:   

*destination* A hash reference with one property: *amount*

:   

*receipt\_email* E-mail address

:   

*statement\_descriptor* Text

:   

*statement\_descriptor\_suffix* String

:   

*transfer\_data* A hash reference with one property: *amount*

:   

*transfer\_group* Text

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/charges/capture>

### list

This will list all the charges for a given customer and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*created* A date that can also be expressed as a unix timestamp

:   

*customer* A Stripe customer id

:   

*ending\_before* A Stripe charge id

:   

*limit* Integer

:   

*payment\_intent* A payment intent Stripe id

:   

*source* A source Stripe id

:   

*starting\_after* A Stripe charge id

:   

*transfer\_group* Text

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/charges/list>

COUPONS
-------

You can **create**, **retrieve**, **update**, **delete** or **list**
coupons.

### create

Provided with a
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}
object or a hash reference, this will create a Stripe coupon and return
a coupon object
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}

Possible parameters are:

*duration* String that can be forever, once or repeating

:   

*amount\_off* Integer

:   

*currency* Three-letters iso 4217 currency code such as `jpy` for Japanese Yen

:   

*duration\_in\_months* Integer

:   

*id* A Coupon id, which is also the coupon code, so you are encouraged to create it

:   

*max\_redemptions* Integer

:   

*metadata* Arbitrary hash reference

:   

*name* String

:   

*percent\_off* Percentage such as \> 0 and \<= 100

:   

*redeem\_by* Date

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/coupons/create>

### retrieve

Provided with a
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}
object or a hash reference, this will retrieve a Stripe coupon and
return a coupon object
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}.

Possible parameters are:

*id* A Stripe coupon id

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/coupons/retrieve>

### update

Provided with a
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}
object or a hash reference, this will update a Stripe coupon and return
a coupon object
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}.

Possible parameters are:

*id* A Stripe coupon id

:   

*metadata* Arbitrary hash reference

:   

*name* String

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/coupons/update>

### delete

Provided with a
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}
object or a hash reference, this will remove the Stripe coupon and
return a coupon object
[Net::API::Stripe::Billing::Coupon](https://metacpan.org/pod/Net::API::Stripe::Billing::Coupon){.perl-module}
with the property *deleted* set to true.

For more information see Stripe documentation here:
<https://stripe.com/docs/api/coupons/delete>

### list

This will list all the coupons and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*created* A date that can also be expressed as a unix timestamp

:   

*customer* A Stripe customer id

:   

*ending\_before* A Stripe charge id

:   

*limit* Integer

:   

*starting\_after* A Stripe charge id

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/coupons/list>

CREDIT NOTES
------------

You can **preview**, **create**, **lines**, **lines\_preview**,
**retrieve**, **update**, **void** or **list** credit notes.

### preview

Provided with a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
or a hash reference, this will create a Stripe credit note preview and
return a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object.

*invoice* A Stripe invoice id. This is required.

:   

*amount* Integer

:   

*credit\_amount* Integer

:   

*lines* An array of hash with properties: amount description invoice\_line\_item quantity tax\_rates type unit\_amount unit\_amount\_decimal

:   

*memo* Text

:   

*metadata* Arbitrary hash reference

:   

*out\_of\_band\_amount* Integer

:   

*reason* Text

:   

*refund* A Stripe refund id

:   

*refund\_amount* Integer

:   

### create

Provided with a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object or a hash reference, this will create a Stripe credit note and
return a credit note
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object.

Possible parameters are:

*invoice* A Stripe invoice id. This is required.

:   

*amount* Integer

:   

*credit\_amount* Integer

:   

*lines* An array of hash with properties: amount description invoice\_line\_item quantity tax\_rates type unit\_amount unit\_amount\_decimal

:   

*memo* Text

:   

*metadata* Arbitrary hash reference

:   

*out\_of\_band\_amount* Integer

:   

*reason* Text

:   

*refund* A Stripe refund id

:   

*refund\_amount* Integer

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/credit_notes/create>

### lines

Provided with a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object or a hash reference, this gets the list of all the credit note
line items and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*id* A Stripe credit note id. This is required.

:   

*ending\_before* A Stripe credit note id.

:   

*limit* Integer

:   

*starting\_after* A Stripe credit note id.

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/credit_notes/lines>

### lines\_preview

Provided with a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object or a hash reference, this gets the list of all the credit note
preview line items and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*amount* Integer

:   

*credit\_amount* Integer

:   

*ending\_before* A Stripe credit note id.

:   

*invoice* A Stripe invoice id. This is required.

:   

*limit* Integer

:   

*lines* An array of hash with properties: amount description invoice\_line\_item quantity tax\_rates type unit\_amount unit\_amount\_decimal

:   

*memo* Text

:   

*metadata* Arbitrary hash reference

:   

*out\_of\_band\_amount* Integer

:   

*reason* Text

:   

*refund* A Stripe refund id

:   

*refund\_amount* Integer

:   

*starting\_after* A Stripe credit note id.

:   

For more information see Stripe documentation here:
<https://stripe.com/docs/api/credit_notes/lines>

### retrieve

Provided with a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object or a hash reference, this will retrieve the Stripe credit note
and return a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object

Possible parameters are:

*id* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/credit_notes/retrieve>

### update

Provided with a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object or a hash reference, this will update a Stripe credit note and
return a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object

Possible parameters are:

*id* A Stripe credit note id. This is required

:   

*memo* Text

:   

*metadata* Arbitrary hash reference

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/credit_notes/update>

### void

Provided with a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object or a hash reference, this will void a Stripe credit note and
return a
[Net::API::Stripe::Billing::CreditNote](https://metacpan.org/pod/Net::API::Stripe::Billing::CreditNote){.perl-module}
object

Possible parameters are:

*id* A Stripe credit note id. This is required

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/credit_notes/void>

### list

Given a set of optional parameters, this get the list of credit notes
and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

*created* Date or unix timestamp

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/credit_notes/list>

CUSTOMERS
---------

You can **create**, **retrieve**, **update**, **delete**,
**delete\_discount** or **list** customers.

### create

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object or a hash reference, this will create a Stripe customer and
return its
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object.

Possible parameters are:

*account\_balance* Integer

:   

*address* A [Net::API::Stripe::Address](https://metacpan.org/pod/Net::API::Stripe::Address){.perl-module} object or a hash reference with the following properties: line1 city country line2 postal\_code state

:   

*balance* Integer

:   

*coupon* A string that matches an existing Stripe coupon.

:   

*default\_source*

:   

*description* Test

:   

*email* String

:   

*id* A customer id, or Stripe will create one

:   

*invoice\_prefix* String

:   

*invoice\_settings* A hash reference with the following properties: custom\_fields default\_payment\_method footer

:   

*metadata* An arbitrary hash reference

:   

*name* String. Customer name

:   

*payment\_method* A Stripe payment method id

:   

*phone* String.

:   

*preferred\_locales* An array of strings representing 2-letters ISO 639 language codes such as `[qw( en fr ja )]`

:   

*shipping* A [Net::API::Stripe::Address](https://metacpan.org/pod/Net::API::Stripe::Address){.perl-module} object or a hash reference with the following properties: line1 city country line2 postal\_code state

:   

*source*

:   

*tax\_exempt* String that can be either *none*, *exempt* or *reverse*

:   

*tax\_id\_data* An array reference of string representing tax id or [Net::API::Stripe::Customer::TaxId](https://metacpan.org/pod/Net::API::Stripe::Customer::TaxId){.perl-module} objects

:   

*tax\_info* A [Net::API::Stripe::Customer::TaxInfo](https://metacpan.org/pod/Net::API::Stripe::Customer::TaxInfo){.perl-module} object or a hash reference with the following properties: tax\_id type

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customers/create>

### retrieve

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object or a hash reference, this will retrieve a Stripe customer and
return its
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object.

Possible parameters are:

*id* A Stripe customer id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customers/retrieve>

### update

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object or a hash reference, this will update a Stripe customer and
return its
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object.

Possible parameters are:

*account\_balance* Integer

:   

*address* A [Net::API::Stripe::Address](https://metacpan.org/pod/Net::API::Stripe::Address){.perl-module} object or a hash reference with the following properties: line1 city country line2 postal\_code state

:   

*balance* Integer

:   

*coupon* A string that matches an existing Stripe coupon.

:   

*default\_source*

:   

*description* Test

:   

*email* String

:   

*id* A customer id, or Stripe will create one

:   

*invoice\_prefix* String

:   

*invoice\_settings* A hash reference with the following properties: custom\_fields default\_payment\_method footer

:   

*metadata* An arbitrary hash reference

:   

*name* String. Customer name

:   

*next\_invoice\_sequence* String

:   

*payment\_method* A Stripe payment method id

:   

*phone* String.

:   

*preferred\_locales* An array of strings representing 2-letters ISO 639 language codes such as `[qw( en fr ja )]`

:   

*shipping* A [Net::API::Stripe::Address](https://metacpan.org/pod/Net::API::Stripe::Address){.perl-module} object or a hash reference with the following properties: line1 city country line2 postal\_code state

:   

*source*

:   

*tax\_exempt* String that can be either *none*, *exempt* or *reverse*

:   

*tax\_id\_data* An array reference of string representing tax id or [Net::API::Stripe::Customer::TaxId](https://metacpan.org/pod/Net::API::Stripe::Customer::TaxId){.perl-module} objects

:   

*tax\_info* A [Net::API::Stripe::Customer::TaxInfo](https://metacpan.org/pod/Net::API::Stripe::Customer::TaxInfo){.perl-module} object or a hash reference with the following properties: tax\_id type

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customers/create>

### delete

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object or a hash reference, this will remove a Stripe customer and
return its
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object with the property *deleted* set to true.

Possible parameters are:

*id* A Stripe customer id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customers/delete>

### delete\_discount

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object or a hash reference, this will remove a Stripe customer discount
and return the discount removed as a
[Net::API::Stripe::Billing::Discount](https://metacpan.org/pod/Net::API::Stripe::Billing::Discount){.perl-module}
object.

Possible parameters are:

*id* A Stripe customer id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/discounts/delete>

### list

Provided with some optional parameters, this will get a list of Stripe
customers and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*created* Date or unix timestamp

:   

*email* String. E-mail address

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customers/list>

DISCOUNTS
---------

You can execute the following options: **delete\_customer** or
**delete\_subscription**, such as:

        $stripe->discounts( delete_customer => { customer => $customer_id, id => $discount_id });

They will call respectively `$self-`customers( delete\_discount =\> \@\_
)\> and `$self-`subscriptions( delete\_discount =\> \@\_ )\>

DISPUTES
--------

You can **close**, **retrieve**, **update** or **list** disputes

### close

Provided with a
[Net::API::Stripe::Dispute](https://metacpan.org/pod/Net::API::Stripe::Dispute){.perl-module}
object or an hash reference and this will close a Stripe dispute and
return
[Net::API::Stripe::Dispute](https://metacpan.org/pod/Net::API::Stripe::Dispute){.perl-module}
object.

Possible parameters are:

*id* A Stripe dispute id.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/disputes/close>

### retrieve

Provided with a
[Net::API::Stripe::Dispute](https://metacpan.org/pod/Net::API::Stripe::Dispute){.perl-module}
or a hash reference of parameters, this will retrieve the Stripe dispute
and return a
[Net::API::Stripe::Dispute](https://metacpan.org/pod/Net::API::Stripe::Dispute){.perl-module}
object.

Possible parameters are:

*id* A Stripe dispute id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/disputes/retrieve>

### update

Provided with a
[Net::API::Stripe::Dispute](https://metacpan.org/pod/Net::API::Stripe::Dispute){.perl-module}
or a hash reference of parameters, this will update a Stripe dispute and
return a
[Net::API::Stripe::Dispute](https://metacpan.org/pod/Net::API::Stripe::Dispute){.perl-module}
object.

Possible parameters are:

*id* A Stripe dispute id. This is required.

:   

*evidence* This is a hash reference with following possible properties:

:   

    *access\_activity\_log*

    :   

    *billing\_address*

    :   

    *cancellation\_policy*

    :   

    *cancellation\_policy\_disclosure*

    :   

    *cancellation\_rebuttal*

    :   

    *customer\_communication*

    :   

    *customer\_email\_address*

    :   

    *customer\_name*

    :   

    *customer\_purchase\_ip*

    :   

    *customer\_signature*

    :   

    *duplicate\_charge\_documentation*

    :   

    *duplicate\_charge\_explanation*

    :   

    *duplicate\_charge\_id*

    :   

    *product\_description*

    :   

    *receipt*

    :   

    *refund\_policy*

    :   

    *refund\_policy\_disclosure*

    :   

    *refund\_refusal\_explanation*

    :   

    *service\_date*

    :   

    *service\_documentation*

    :   

    *shipping\_address*

    :   

    *shipping\_carrier*

    :   

    *shipping\_date*

    :   

    *shipping\_documentation*

    :   

    *shipping\_tracking\_number*

    :   

    *uncategorized\_file*

    :   

    *uncategorized\_text*

    :   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/disputes/update>

### list

Provided with some optional parameters and this will issue a Stripe api
call to get the list of disputes and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*created* Date or unix timestamp

:   

*charge* A Stripe charge id

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*payment\_intent* A Stripe payment intent id

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/disputes/list>

FILES
-----

You can **create**, **retrieve** or **list** files

### create

Provided with a
[Net::API::Stripe::File](https://metacpan.org/pod/Net::API::Stripe::File){.perl-module}
or a hash reference of parameters, this will create a Stripe file and
return a
[Net::API::Stripe::File](https://metacpan.org/pod/Net::API::Stripe::File){.perl-module}
object.

Possible parameters are:

*file* File path.

:   It will check if the file exists, is not zero length, is readable
    and make the file path absolute if it is relative (using
    `Cwd::abs_path`)

*file\_link\_data* A hash reference with the following properties: create expires\_at metadata

:   

*purpose* String that can be either *business\_icon* *business\_logo* *customer\_signature* *dispute\_evidence* *identity\_document* *pci\_document* or *tax\_document\_user\_upload*

:   

For more information, see Stripe documentation here:
[httpshttps://stripe.com/docs/api/files/create](httpshttps://stripe.com/docs/api/files/create){.perl-module}

### retrieve

Provided with a
[Net::API::Stripe::File](https://metacpan.org/pod/Net::API::Stripe::File){.perl-module}
or a hash reference of parameters, this will retrieve the Stripe file
and return a
[Net::API::Stripe::File](https://metacpan.org/pod/Net::API::Stripe::File){.perl-module}
object.

Possible parameters are:

*id* A Stripe file id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/files/retrieve>

### list

Provided with some optional parameters and this will issue a Stripe api
call to get the list of files and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*created* Date or unix timestamp

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*purpose* String.

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/files/list>

INVOICES
--------

You can **create** **delete** **finalise** **lines** **lines\_upcoming**
**invoice\_write\_off** **upcoming** **pay** **retrieve** **send**
**update** **void** **list** invoices

### create

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference, this will create a Stripe invoice and
return a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*customer* A Stripe customer id. This is required.

:   

*application\_fee\_amount* Integer

:   

*auto\_advance* Boolean

:   

*collection\_method* String. Either `charge_automatically`, or `send_invoice`

:   

*custom\_fields* An array of hash reference with key and value properties.

:   

*days\_until\_due* Integer

:   

*default\_payment\_method* A Stripe payment method id

:   

*default\_source* String

:   

*default\_tax\_rates* Array reference of decimal amount

:   

*description* Text

:   

*due\_date* Date or unix timestamp

:   

*footer* Text

:   

*metadata* An arbitrary hash reference

:   

*statement\_descriptor* Text

:   

*subscription* A Stripe subscription id

:   

*tax\_percent* Decimal value

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/create>

### delete

This is to remove draft invoice. When provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will remove the draft
invoice and return a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*id* A Stripe draft invoice id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/delete>

### finalise

When provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will set the draft
invoice as finalised and return a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*id* A Stripe draft invoice id

:   

*auto\_advance* Boolean

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/finalize>

### lines

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will retrieve the list
of invoice lines and return a
[Net:API::Stripe::List](Net:API::Stripe::List){.perl-module}

Possible parameters are:

*id* A Stripe invoice id. This is required.

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/invoice_lines>

### lines\_upcoming

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will retrieve the list
of upcoming invoice lines and return a
[Net:API::Stripe::List](Net:API::Stripe::List){.perl-module}

Possible parameters are:

*customer* A Stripe customer id. This is required

:   

*coupon* String

:   

*ending\_before* A Stripe invoice id

:   

*invoice\_items* An array of hash with the following properties:

:   

    *amount*

    :   

    *currency*

    :   

    *description*

    :   

    *discountable*

    :   

    *invoiceitem*

    :   

    *metadata*

    :   

    *period.end*

    :   

    *period.start*

    :   

    *quantity*

    :   

    *tax\_rates*

    :   

    *unit\_amount*

    :   

    *unit\_amount\_decimal*

    :   

*limit* Integer

:   

*schedule* A Stripe schedule id

:   

*starting\_after* A Stripe invoice id

:   

*subscription* A Stripe subscription id

:   

*subscription\_billing\_cycle\_anchor* A timestamp

:   

*subscription\_cancel\_at* A timestamp

:   

*subscription\_cancel\_at\_period\_end* Boolean

:   

*subscription\_cancel\_now* Boolean

:   

*subscription\_default\_tax\_rates* Array of tax rates

:   

*subscription\_items* List of subscription items, each with an attached plan.

:   

*subscription\_prorate* String. If previewing an update to a subscription, this decides whether the preview will show the result of applying prorations or not. If set, one of subscription\_items or subscription, and one of subscription\_items or subscription\_trial\_end are required.

:   

*subscription\_proration\_behavior* String. Determines how to handle prorations when the billing cycle changes.

:   

*subscription\_proration\_date* Date/timestamp. If previewing an update to a subscription, and doing proration, subscription\_proration\_date forces the proration to be calculated as though the update was done at the specified time.

:   

*subscription\_start\_date* Date/timestamp.

:   

*subscription\_tax\_percent* Decimal

:   

*subscription\_trial\_end* Boolean. If set, one of subscription\_items or subscription is required.

:   

*subscription\_trial\_from\_plan* Boolean. Indicates if a plan's trial\_period\_days should be applied to the subscription. Setting this flag to true together with subscription\_trial\_end is not allowed.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/upcoming_invoice_lines>

### invoice\_write\_off

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will write off an
invoice and return its
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*id* A Stripe invoice id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/mark_uncollectible>

### upcoming

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will retrieve an
upcoming invoice and return its
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*customer* A Stripe customer id. This is required

:   

*coupon* String

:   

*invoice\_items* An array of hash with the following properties:

:   

    *amount*

    :   

    *currency*

    :   

    *description*

    :   

    *discountable*

    :   

    *invoiceitem*

    :   

    *metadata*

    :   

    *period.end*

    :   

    *period.start*

    :   

    *quantity*

    :   

    *tax\_rates*

    :   

    *unit\_amount*

    :   

    *unit\_amount\_decimal*

    :   

*schedule* A Stripe schedule id

:   

*subscription* A Stripe subscription id

:   

*subscription\_billing\_cycle\_anchor* A timestamp

:   

*subscription\_cancel\_at* A timestamp

:   

*subscription\_cancel\_at\_period\_end* Boolean

:   

*subscription\_cancel\_now* Boolean

:   

*subscription\_default\_tax\_rates* Array of tax rates

:   

*subscription\_items* List of subscription items, each with an attached plan.

:   

*subscription\_prorate* String. If previewing an update to a subscription, this decides whether the preview will show the result of applying prorations or not. If set, one of subscription\_items or subscription, and one of subscription\_items or subscription\_trial\_end are required.

:   

*subscription\_proration\_behavior* String. Determines how to handle prorations when the billing cycle changes.

:   

*subscription\_proration\_date* Date/timestamp. If previewing an update to a subscription, and doing proration, subscription\_proration\_date forces the proration to be calculated as though the update was done at the specified time.

:   

*subscription\_start\_date* Date/timestamp.

:   

*subscription\_tax\_percent* Decimal

:   

*subscription\_trial\_end* Boolean. If set, one of subscription\_items or subscription is required.

:   

*subscription\_trial\_from\_plan* Boolean. Indicates if a plan's trial\_period\_days should be applied to the subscription. Setting this flag to true together with subscription\_trial\_end is not allowed.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/upcoming>

### pay

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will mark an invoice as
paid and return its
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*id* A Stripe invoice id. This is required.

:   

*forgive* Boolean

:   

*off\_session* Boolean

:   

*paid\_out\_of\_band* Boolean to signify this was paid outside of Stripe

:   

*payment\_method* A Stripe payment method id

:   

*source* A Stripe source id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/pay>

### retrieve

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will retrieve an invoice
and return its
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*id* A Stripe invoice id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/retrieve>

### send

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will send an invoice to
a recipient to get payment and return its
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*id* A Stripe invoice id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/send>

### update

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will update an invoice
and return its
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*id*

:   

*application\_fee\_amount* Integer

:   

*auto\_advance* Boolean

:   

*collection\_method* String. Either `charge_automatically`, or `send_invoice`

:   

*custom\_fields* An array of hash reference with key and value properties.

:   

*days\_until\_due* Integer

:   

*default\_payment\_method* A Stripe payment method id

:   

*default\_source* String

:   

*default\_tax\_rates* Array reference of decimal amount

:   

*description* Text

:   

*due\_date* Date or unix timestamp

:   

*footer* Text

:   

*metadata* An arbitrary hash reference

:   

*statement\_descriptor* Text

:   

*tax\_percent* Decimal value

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/update>

### void

Provided with a
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object or an hash reference of parameters, this will void (i.e. cancel)
an invoice and return its
[Net::API::Stripe::Billing::Invoice](https://metacpan.org/pod/Net::API::Stripe::Billing::Invoice){.perl-module}
object.

Possible parameters are:

*id* A Stripe invoice id. This is required.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/void>

### list

Provided with an hash reference of parameters, this returns a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*collection\_method* String that can be charge\_automatically or send\_invoice.

:   

*created* Date or unix timestamp

:   

*customer* A Stripe customer id.

:   

*due\_date* Date / timestamp

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*starting\_after* A Stripe credit note id

:   

*status* String. Status of the invoice, which can be one of draft, open, paid, uncollectible and void

:   

*subscription* A Stripe subscription id.

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/invoices/list>

PAYMENT INTENTS
---------------

You can **create** **retrieve** **update** **confirm** **capture**
**cancel** **list** payment intents

### create

Provided with a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object or an hash reference, this will create a Stripe payment intent
and return a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object.

Possible parameters are:

*amount* Integer

:   Amount intended to be collected by this PaymentIntent. A positive
    integer representing how much to charge in the smallest currency
    unit (e.g., 100 cents to charge \$1.00 or 100 to charge ¥100, a
    zero-decimal currency). The minimum amount is \$0.50 US or
    equivalent in charge currency. The amount value supports up to eight
    digits (e.g., a value of 99999999 for a USD charge of \$999,999.99).

*application\_fee\_amount* Integer

:   The amount of the application fee (if any) that will be requested to
    be applied to the payment and transferred to the application owner's
    Stripe account. The amount of the application fee collected will be
    capped at the total payment amount. For more information, see the
    PaymentIntents use case for connected accounts.

*capture\_method* String

:   Controls when the funds will be captured from the customer's
    account.

    Possible enum values

    *automatic* String

    :   (Default) Stripe automatically captures funds when the customer
        authorizes the payment.

    *manual* String

    :   Place a hold on the funds when the customer authorizes the
        payment, but don't capture the funds until later. (Not all
        payment methods support this.)

*confirmation\_method* String

:   Possible enum values

    *automatic* String

    :   (Default) PaymentIntent can be confirmed using a publishable
        key. After next\_actions are handled, no additional confirmation
        is required to complete the payment.

    *manual* String

    :   All payment attempts must be made using a secret key. The
        PaymentIntent returns to the requires\_confirmation state after
        handling next\_actions, and requires your server to initiate
        each payment attempt with an explicit confirmation.

*currency* String

:   Three-letter ISO currency code, in lowercase. Must be a supported
    currency.

*confirm* Boolean

:   Set to true to attempt to confirm this PaymentIntent immediately.
    This parameter defaults to false. When creating and confirming a
    PaymentIntent at the same time, parameters available in the confirm
    API may also be provided.

*customer* String.

:   ID of the Customer this PaymentIntent belongs to, if one exists.

    Payment methods attached to other Customers cannot be used with this
    PaymentIntent.

    If present in combination with setup\_future\_usage, this
    PaymentIntent's payment method will be attached to the Customer
    after the PaymentIntent has been confirmed and any required actions
    from the user are complete.

*description* String

:   An arbitrary string attached to the object. Often useful for
    displaying to users.

*error\_on\_requires\_action* Boolean

:   Set to true to fail the payment attempt if the PaymentIntent
    transitions into requires\_action. This parameter is intended for
    simpler integrations that do not handle customer actions, like
    saving cards without authentication. This parameter can only be used
    with confirm=true.

*mandate* String

:   ID of the mandate to be used for this payment. This parameter can
    only be used with confirm=true.

*mandate\_data* Hash

:   This hash contains details about the Mandate to create. This
    parameter can only be used with confirm=true.

    *customer\_acceptance* Hash required

    :   This hash contains details about the customer acceptance of the
        Mandate.

        *type* String required

        :   The type of customer acceptance information included with
            the Mandate. One of online or offline.

        *accepted\_at* Datetime

        :   The time at which the customer accepted the Mandate.

        *offline* Hash

        :   If this is a Mandate accepted offline, this hash contains
            details about the offline acceptance.

        *online* Hash

        :   If this is a Mandate accepted online, this hash contains
            details about the online acceptance.

            *ip\_address* String

            :   The IP address from which the Mandate was accepted by
                the customer.

            *user\_agent* String

            :   The user agent of the browser from which the Mandate was
                accepted by the customer.

*metadata* Hash

:   Set of key-value pairs that you can attach to an object. This can be
    useful for storing additional information about the object in a
    structured format. Individual keys can be unset by posting an empty
    value to them. All keys can be unset by posting an empty value to
    metadata.

*on\_behalf\_of* String

:   The Stripe account ID for which these funds are intended. For
    details, see the PaymentIntents use case for connected accounts.

*off\_session* String

:   Set to true to indicate that the customer is not in your checkout
    flow during this payment attempt, and therefore is unable to
    authenticate. This parameter is intended for scenarios where you
    collect card details and charge them later. This parameter can only
    be used with confirm=true.

*payment\_method* String

:   ID of the payment method (a PaymentMethod, Card, or compatible
    Source object) to attach to this PaymentIntent.

    If this parameter is omitted with confirm=true,
    customer.default\_source will be attached as this PaymentIntent's
    payment instrument to improve the migration experience for users of
    the Charges API. We recommend that you explicitly provide the
    payment\_method going forward.

*payment\_method\_data* Hash

:   If provided, this hash will be used to create a PaymentMethod. The
    new PaymentMethod will appear in the payment\_method property on the
    PaymentIntent.

    *alipay* Hash

    :   If this is an Alipay PaymentMethod, this hash contains details
        about the Alipay payment method.

        No documented property.

    *au\_becs\_debit* Hash

    :   If this is an au\_becs\_debit PaymentMethod, this hash contains
        details about the bank account.

        *account\_number* String

        :   The account number for the bank account

        *bsb\_number* String

        :   Bank-State-Branch number of the bank account.

    *bacs\_debit* Hash

    :   If this is a bacs\_debit PaymentMethod, this hash contains
        details about the Bacs Direct Debit bank account.

        *account\_number* String

        :   Account number of the bank account that the funds will be
            debited from.

        *sort\_code* String

        :   Sort code of the bank account. (e.g., 10-20-30)

    *bancontact* Hash

    :   If this is a bancontact PaymentMethod, this hash contains
        details about the Bancontact payment method.

        No documented property.

    *billing\_details* Hash

    :   Billing information associated with the PaymentMethod that may
        be used or required by particular types of payment methods.

        *address* Hash

        :   Billing address.

            *city* String

            :   City, district, suburb, town, or village.

            *country* String

            :   Two-letter country code (ISO 3166-1 alpha-2).

            *line1* String

            :   Address line 1 (e.g., street, PO Box, or company name).

            *line2* String

            :   Address line 2 (e.g., apartment, suite, unit, or
                building).

            *postal\_code* String

            :   ZIP or postal code.

            *state* String

            :   State, county, province, or region.

        *email* String

        :   Email address.

        *name* String

        :   Full name.

        *phone* String

        :   Billing phone number (including extension).

    *eps* hash

    :   If this is an eps PaymentMethod, this hash contains details
        about the EPS payment method.

        No documented property.

    *fpx* Hash

    :   If this is an fpx PaymentMethod, this hash contains details
        about the FPX payment method.

        *bank* String

        :   The customer's bank.

    *giropay* Hash

    :   If this is a giropay PaymentMethod, this hash contains details
        about the Giropay payment method.

        No documented property.

    *grabpay* Hash

    :   If this is a grabpay PaymentMethod, this hash contains details
        about the GrabPay payment method.

        No documented property.

    *ideal* Hash

    :   If this is an ideal PaymentMethod, this hash contains details
        about the iDEAL payment method.

        *bank* String

        :   The customer's bank.

    *interac\_present* Hash

    :   If this is an interac\_present PaymentMethod, this hash contains
        details about the Interac Present payment method.

        No documented property.

    *metadata* Hash

    :   Set of key-value pairs that you can attach to an object. This
        can be useful for storing additional information about the
        object in a structured format. Individual keys can be unset by
        posting an empty value to them. All keys can be unset by posting
        an empty value to metadata.

    *oxxo* Hash

    :   If this is an oxxo PaymentMethod, this hash contains details
        about the OXXO payment method.

        No documented property.

    *p24* Hash

    :   If this is a p24 PaymentMethod, this hash contains details about
        the P24 payment method.

        *bank* String

        :   The customer's bank.

    *sepa\_debit* Hash

    :   If this is a sepa\_debit PaymentMethod, this hash contains
        details about the SEPA debit bank account.

        *iban* String required

        :   IBAN of the bank account.

    *sofort* Hash

    :   If this is a sofort PaymentMethod, this hash contains details
        about the SOFORT payment method.

        *country*

        :   Two-letter ISO code representing the country the bank
            account is located in.

    *type* String

    :   The type of the PaymentMethod. An additional hash is included on
        the PaymentMethod with a name matching this value. It contains
        additional information specific to the PaymentMethod type.

*payment\_method\_options* Hash

:   Payment-method-specific configuration for this PaymentIntent.

    *alipay* Hash

    :   If this is a alipay PaymentMethod, this sub-hash contains
        details about the Alipay payment method options.

        No documented property.

    *bancontact* Hash

    :   If this is a bancontact PaymentMethod, this sub-hash contains
        details about the Bancontact payment method options.

        *preferred\_language* String

        :   Preferred language of the Bancontact authorization page that
            the customer is redirected to.

    *card* Hash

    :   Configuration for any card payments attempted on this
        PaymentIntent.

        *cvc\_token*

        :   A single-use cvc\_update Token that represents a card CVC
            value. When provided, the CVC value will be verified during
            the card payment attempt. This parameter can only be
            provided during confirmation.

        *installments*

        :   Installment configuration for payments attempted on this
            PaymentIntent (Mexico Only).

            For more information, see the [installments integration
            guide](https://stripe.com/docs/payments/installments){.perl-module}.

            *enabled* Boolean

            :   Setting to true enables installments for this
                PaymentIntent. This will cause the response to contain a
                list of available installment plans. Setting to false
                will prevent any selected plan from applying to a
                charge.

            *plan* Hash

            :   The selected installment plan to use for this payment
                attempt. This parameter can only be provided during
                confirmation.

                *count* Integer required

                :   For fixed\_count installment plans, this is the
                    number of installment payments your customer will
                    make to their credit card.

                *interval* String required

                :   For fixed\_count installment plans, this is the
                    interval between installment payments your customer
                    will make to their credit card. One of month.

                *type* String required

                :   Type of installment plan, one of fixed\_count.

        *network* String

        :   Selected network to process this PaymentIntent on. Depends
            on the available networks of the card attached to the
            PaymentIntent. Can be only set confirm-time.

        *request\_three\_d\_secure* String

        :   We strongly recommend that you rely on our SCA Engine to
            automatically prompt your customers for authentication based
            on risk level and other requirements. However, if you wish
            to request 3D Secure based on logic from your own fraud
            engine, provide this option. Permitted values include:
            automatic or any. If not provided, defaults to automatic.
            Read our guide on manually requesting 3D Secure for more
            information on how this configuration interacts with Radar
            and our SCA Engine.

    *oxxo* Hash

    :   If this is a oxxo PaymentMethod, this sub-hash contains details
        about the OXXO payment method options.

        *expires\_after\_days* Integer

        :   The number of calendar days before an OXXO voucher expires.
            For example, if you create an OXXO voucher on Monday and you
            set expires\_after\_days to 2, the OXXO invoice will expire
            on Wednesday at 23:59 America/Mexico\_City time.

    *p24* Hash

    :   If this is a p24 PaymentMethod, this sub-hash contains details
        about the Przelewy24 payment method options.

        No property documented.

    *sepa\_debit* Hash

    :   If this is a sepa\_debit PaymentIntent, this sub-hash contains
        details about the SEPA Debit payment method options.

        *mandate\_options* Hash

        :   Additional fields for Mandate creation

            No property provided.

    *sofort* Hash

    :   If this is a sofort PaymentMethod, this sub-hash contains
        details about the SOFORT payment method options.

        *preferred\_language* String

        :   Language shown to the payer on redirect.

*payment\_method\_types* array of strings

:   The list of payment method types that this PaymentIntent is allowed
    to use. If this is not provided, defaults to \["card"\]. Valid
    payment method types include: alipay, au\_becs\_debit, bancontact,
    card, card\_present, eps, giropay, ideal, interac\_present, p24,
    sepa\_debit, and sofort.

*receipt\_email* String

:   Email address that the receipt for the resulting payment will be
    sent to. If receipt\_email is specified for a payment in live mode,
    a receipt will be sent regardless of your email settings.

*return\_url* String

:   The URL to redirect your customer back to after they authenticate or
    cancel their payment on the payment method's app or site. If you'd
    prefer to redirect to a mobile application, you can alternatively
    supply an application URI scheme. This parameter can only be used
    with confirm=true.

*setup\_future\_usage* String

:   Indicates that you intend to make future payments with this
    PaymentIntent's payment method.

    Providing this parameter will attach the payment method to the
    PaymentIntent's Customer, if present, after the PaymentIntent is
    confirmed and any required actions from the user are complete. If no
    Customer was provided, the payment method can still be attached to a
    Customer after the transaction completes.

    When processing card payments, Stripe also uses setup\_future\_usage
    to dynamically optimize your payment flow and comply with regional
    legislation and network rules, such as SCA.

    Possible enum values

    *on\_session* String

    :   Use on\_session if you intend to only reuse the payment method
        when your customer is present in your checkout flow.

    *off\_session* String

    :   Use off\_session if your customer may or may not be present in
        your checkout flow.

*shipping* Hash

:   Shipping information for this PaymentIntent.

    *address* Hash required

    :   Shipping address.

        *city* String

        :   City, district, suburb, town, or village.

        *country* String

        :   Two-letter country code (ISO 3166-1 alpha-2).

        *line1* String required

        :   Address line 1 (e.g., street, PO Box, or company name).

        *line2* String

        :   Address line 2 (e.g., apartment, suite, unit, or building).

        *postal\_code* String

        :   ZIP or postal code.

        *state* String

        :   State, county, province, or region.

    *name* String required

    :   Recipient name.

    *carrier* String

    :   The delivery service that shipped a physical product, such as
        Fedex, UPS, USPS, etc.

    *phone* String

    :   Recipient phone (including extension).

    *tracking\_number* String

    :   The tracking number for a physical product, obtained from the
        delivery service. If multiple tracking numbers were generated
        for this purchase, please separate them with commas.

*statement\_descriptor* String

:   For non-card charges, you can use this value as the complete
    description that appears on your customers' statements. Must contain
    at least one letter, maximum 22 characters.

*statement\_descriptor\_suffix* Text

:   Provides information about a card payment that customers see on
    their statements. Concatenated with the prefix (shortened
    descriptor) or statement descriptor that's set on the account to
    form the complete statement descriptor. Maximum 22 characters for
    the concatenated descriptor.

*transfer\_data* Hash

:   The parameters used to automatically create a Transfer when the
    payment succeeds. For more information, see the PaymentIntents use
    case for connected accounts.

    *destination* String required

    :   If specified, successful charges will be attributed to the
        destination account for tax reporting, and the funds from
        charges will be transferred to the destination account. The ID
        of the resulting transfer will be returned on the successful
        charge's transfer field.

    *amount* Integer

    :   The amount that will be transferred automatically when a charge
        succeeds. The amount is capped at the total transaction amount
        and if no amount is set, the full amount is transferred.

        If you intend to collect a fee and you need a more robust
        reporting experience, using application\_fee\_amount might be a
        better fit for your integration.

*transfer\_group* String

:   A string that identifies the resulting payment as part of a group.
    See the PaymentIntents use case for connected accounts for details.

*use\_stripe\_sdk* Boolean

:   Set to true only when using manual confirmation and the iOS or
    Android SDKs to handle additional authentication steps.

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_intents/create>

### retrieve

Provided with a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object or an hash reference, this will retrieve a Stripe payment intent
and return a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object.

Possible parameters are:

*client\_secret* String required

:   The client secret of the PaymentIntent. Required if a publishable
    key is used to retrieve the source.

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_intents/retrieve>

### update

Provided with a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object or an hash reference, this will update the Stripe payment intent
and return a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object.

Possible parameters are:

*amount* Integer

:   Amount intended to be collected by this PaymentIntent. A positive
    integer representing how much to charge in the smallest currency
    unit (e.g., 100 cents to charge \$1.00 or 100 to charge ¥100, a
    zero-decimal currency). The minimum amount is \$0.50 US or
    equivalent in charge currency. The amount value supports up to eight
    digits (e.g., a value of 99999999 for a USD charge of \$999,999.99).

*application\_fee\_amount* Integer

:   The amount of the application fee (if any) that will be requested to
    be applied to the payment and transferred to the application owner's
    Stripe account. The amount of the application fee collected will be
    capped at the total payment amount. For more information, see the
    PaymentIntents use case for connected accounts.

*capture\_method* String

:   Controls when the funds will be captured from the customer's
    account.

    Possible enum values

    *automatic* String

    :   (Default) Stripe automatically captures funds when the customer
        authorizes the payment.

    *manual* String

    :   Place a hold on the funds when the customer authorizes the
        payment, but don't capture the funds until later. (Not all
        payment methods support this.)

*confirmation\_method* String

:   Possible enum values

    *automatic* String

    :   (Default) PaymentIntent can be confirmed using a publishable
        key. After next\_actions are handled, no additional confirmation
        is required to complete the payment.

    *manual* String

    :   All payment attempts must be made using a secret key. The
        PaymentIntent returns to the requires\_confirmation state after
        handling next\_actions, and requires your server to initiate
        each payment attempt with an explicit confirmation.

*currency* String

:   Three-letter ISO currency code, in lowercase. Must be a supported
    currency.

*confirm* Boolean

:   Set to true to attempt to confirm this PaymentIntent immediately.
    This parameter defaults to false. When creating and confirming a
    PaymentIntent at the same time, parameters available in the confirm
    API may also be provided.

*customer* String.

:   ID of the Customer this PaymentIntent belongs to, if one exists.

    Payment methods attached to other Customers cannot be used with this
    PaymentIntent.

    If present in combination with setup\_future\_usage, this
    PaymentIntent's payment method will be attached to the Customer
    after the PaymentIntent has been confirmed and any required actions
    from the user are complete.

*description* String

:   An arbitrary string attached to the object. Often useful for
    displaying to users.

*error\_on\_requires\_action* Boolean

:   Set to true to fail the payment attempt if the PaymentIntent
    transitions into requires\_action. This parameter is intended for
    simpler integrations that do not handle customer actions, like
    saving cards without authentication. This parameter can only be used
    with confirm=true.

*mandate* String

:   ID of the mandate to be used for this payment. This parameter can
    only be used with confirm=true.

*mandate\_data* Hash

:   This hash contains details about the Mandate to create. This
    parameter can only be used with confirm=true.

    *customer\_acceptance* Hash required

    :   This hash contains details about the customer acceptance of the
        Mandate.

        *type* String required

        :   The type of customer acceptance information included with
            the Mandate. One of online or offline.

        *accepted\_at* Datetime

        :   The time at which the customer accepted the Mandate.

        *offline* Hash

        :   If this is a Mandate accepted offline, this hash contains
            details about the offline acceptance.

        *online* Hash

        :   If this is a Mandate accepted online, this hash contains
            details about the online acceptance.

            *ip\_address* String

            :   The IP address from which the Mandate was accepted by
                the customer.

            *user\_agent* String

            :   The user agent of the browser from which the Mandate was
                accepted by the customer.

*metadata* Hash

:   Set of key-value pairs that you can attach to an object. This can be
    useful for storing additional information about the object in a
    structured format. Individual keys can be unset by posting an empty
    value to them. All keys can be unset by posting an empty value to
    metadata.

*on\_behalf\_of* String

:   The Stripe account ID for which these funds are intended. For
    details, see the PaymentIntents use case for connected accounts.

*payment\_method* String

:   ID of the payment method (a PaymentMethod, Card, or compatible
    Source object) to attach to this PaymentIntent.

*payment\_method\_data* Hash

:   If provided, this hash will be used to create a PaymentMethod. The
    new PaymentMethod will appear in the payment\_method property on the
    PaymentIntent.

    *alipay* Hash

    :   If this is an Alipay PaymentMethod, this hash contains details
        about the Alipay payment method.

        No documented property.

    *au\_becs\_debit* Hash

    :   If this is an au\_becs\_debit PaymentMethod, this hash contains
        details about the bank account.

        *account\_number* String

        :   The account number for the bank account.

        *bsb\_number* String

        :   Bank-State-Branch number of the bank account.

    *bacs\_debit* Hash

    :   If this is a bacs\_debit PaymentMethod, this hash contains
        details about the Bacs Direct Debit bank account.

        *account\_number* String

        :   Account number of the bank account that the funds will be
            debited from.

        *sort\_code* String

        :   Sort code of the bank account. (e.g., 10-20-30)

    *bancontact* Hash

    :   If this is a bancontact PaymentMethod, this hash contains
        details about the Bancontact payment method.

        No documented property.

    *billing\_details* Hash

    :   Billing information associated with the PaymentMethod that may
        be used or required by particular types of payment methods.

        *address* Hash

        :   Billing address.

            *city* String

            :   City, district, suburb, town, or village.

            *country* String

            :   Two-letter country code (ISO 3166-1 alpha-2).

            *line1* String

            :   Address line 1 (e.g., street, PO Box, or company name).

            *line2* String

            :   Address line 2 (e.g., apartment, suite, unit, or
                building).

            *postal\_code* String

            :   ZIP or postal code.

            *state* String

            :   State, county, province, or region.

        *email* String

        :   Email address.

        *name* String

        :   Full name.

        *phone* String

        :   Billing phone number (including extension).

    *eps* hash

    :   If this is an eps PaymentMethod, this hash contains details
        about the EPS payment method.

        No documented property.

    *fpx* Hash

    :   If this is an fpx PaymentMethod, this hash contains details
        about the FPX payment method.

        *bank* String required

        :   The customer's bank.

    *giropay* Hash

    :   If this is a giropay PaymentMethod, this hash contains details
        about the Giropay payment method.

        No documented property.

    *grabpay* Hash

    :   If this is a grabpay PaymentMethod, this hash contains details
        about the GrabPay payment method.

        No documented property.

    *ideal* Hash

    :   If this is an ideal PaymentMethod, this hash contains details
        about the iDEAL payment method.

        *bank* String

        :   The customer's bank.

    *interac\_present* Hash

    :   If this is an interac\_present PaymentMethod, this hash contains
        details about the Interac Present payment method.

        No documented property.

    *metadata* Hash

    :   Set of key-value pairs that you can attach to an object. This
        can be useful for storing additional information about the
        object in a structured format. Individual keys can be unset by
        posting an empty value to them. All keys can be unset by posting
        an empty value to metadata.

    *oxxo* Hash

    :   If this is an oxxo PaymentMethod, this hash contains details
        about the OXXO payment method.

        No documented property.

    *p24* Hash

    :   If this is a p24 PaymentMethod, this hash contains details about
        the P24 payment method.

        *bank* String

        :   The customer's bank.

    *sepa\_debit* Hash

    :   If this is a sepa\_debit PaymentMethod, this hash contains
        details about the SEPA debit bank account.

        *iban* String required

        :   IBAN of the bank account.

    *sofort* Hash

    :   If this is a sofort PaymentMethod, this hash contains details
        about the SOFORT payment method.

        *country* String required

        :   Two-letter ISO code representing the country the bank
            account is located in.

    *type* String

    :   The type of the PaymentMethod. An additional hash is included on
        the PaymentMethod with a name matching this value. It contains
        additional information specific to the PaymentMethod type.

*payment\_method\_options* Hash

:   Payment-method-specific configuration for this PaymentIntent.

    *alipay* Hash

    :   If this is a alipay PaymentMethod, this sub-hash contains
        details about the Alipay payment method options.

        No documented property.

    *bancontact* Hash

    :   If this is a bancontact PaymentMethod, this sub-hash contains
        details about the Bancontact payment method options.

        *preferred\_language* String

        :   Preferred language of the Bancontact authorization page that
            the customer is redirected to.

    *card* Hash

    :   Configuration for any card payments attempted on this
        PaymentIntent.

        *cvc\_token*

        :   A single-use cvc\_update Token that represents a card CVC
            value. When provided, the CVC value will be verified during
            the card payment attempt. This parameter can only be
            provided during confirmation.

        *installments*

        :   Installment configuration for payments attempted on this
            PaymentIntent (Mexico Only).

            For more information, see the [installments integration
            guide](https://stripe.com/docs/payments/installments){.perl-module}.

            *enabled* Boolean

            :   Setting to true enables installments for this
                PaymentIntent. This will cause the response to contain a
                list of available installment plans. Setting to false
                will prevent any selected plan from applying to a
                charge.

            *plan* Hash

            :   The selected installment plan to use for this payment
                attempt. This parameter can only be provided during
                confirmation.

                *count* Integer required

                :   For fixed\_count installment plans, this is the
                    number of installment payments your customer will
                    make to their credit card.

                *interval* String required

                :   For fixed\_count installment plans, this is the
                    interval between installment payments your customer
                    will make to their credit card. One of month.

                *type* String required

                :   Type of installment plan, one of fixed\_count.

        *network* String

        :   Selected network to process this PaymentIntent on. Depends
            on the available networks of the card attached to the
            PaymentIntent. Can be only set confirm-time.

        *request\_three\_d\_secure* String

        :   We strongly recommend that you rely on our SCA Engine to
            automatically prompt your customers for authentication based
            on risk level and other requirements. However, if you wish
            to request 3D Secure based on logic from your own fraud
            engine, provide this option. Permitted values include:
            automatic or any. If not provided, defaults to automatic.
            Read our guide on manually requesting 3D Secure for more
            information on how this configuration interacts with Radar
            and our SCA Engine.

    *oxxo* Hash

    :   If this is a oxxo PaymentMethod, this sub-hash contains details
        about the OXXO payment method options.

        *expires\_after\_days* Integer

        :   The number of calendar days before an OXXO voucher expires.
            For example, if you create an OXXO voucher on Monday and you
            set expires\_after\_days to 2, the OXXO invoice will expire
            on Wednesday at 23:59 America/Mexico\_City time.

    *p24* Hash

    :   If this is a p24 PaymentMethod, this sub-hash contains details
        about the Przelewy24 payment method options.

        No property documented.

    *sepa\_debit* Hash

    :   If this is a sepa\_debit PaymentIntent, this sub-hash contains
        details about the SEPA Debit payment method options.

        *mandate\_options* Hash

        :   Additional fields for Mandate creation

            No property provided.

    *sofort* Hash

    :   If this is a sofort PaymentMethod, this sub-hash contains
        details about the SOFORT payment method options.

        *preferred\_language* String

        :   Language shown to the payer on redirect.

*off\_session* String

:   Set to true to indicate that the customer is not in your checkout
    flow during this payment attempt, and therefore is unable to
    authenticate. This parameter is intended for scenarios where you
    collect card details and charge them later. This parameter can only
    be used with confirm=true.

*payment\_method\_types* Array of strings

:   The list of payment method types (e.g. card) that this PaymentIntent
    is allowed to use.

*receipt\_email* String

:   Email address that the receipt for the resulting payment will be
    sent to. If receipt\_email is specified for a payment in live mode,
    a receipt will be sent regardless of your email settings.

*setup\_future\_usage* String

:   Indicates that you intend to make future payments with this
    PaymentIntent's payment method.

    Providing this parameter will attach the payment method to the
    PaymentIntent's Customer, if present, after the PaymentIntent is
    confirmed and any required actions from the user are complete. If no
    Customer was provided, the payment method can still be attached to a
    Customer after the transaction completes.

    When processing card payments, Stripe also uses setup\_future\_usage
    to dynamically optimize your payment flow and comply with regional
    legislation and network rules, such as SCA.

    If setup\_future\_usage is already set and you are performing a
    request using a publishable key, you may only update the value from
    on\_session to off\_session.

    Possible enum values

    *on\_session* String

    :   Use on\_session if you intend to only reuse the payment method
        when your customer is present in your checkout flow.

    *off\_session* String

    :   Use off\_session if your customer may or may not be present in
        your checkout flow.

*return\_url* String

:   The URL to redirect your customer back to after they authenticate or
    cancel their payment on the payment method's app or site. If you'd
    prefer to redirect to a mobile application, you can alternatively
    supply an application URI scheme. This parameter can only be used
    with confirm=true.

*shipping* Hash

:   Shipping information for this PaymentIntent.

    *address* String required

    :   Shipping address.

        *city* String

        :   City, district, suburb, town, or village.

        *country* String

        :   Two-letter country code (ISO 3166-1 alpha-2).

        *line1* String required

        :   Address line 1 (e.g., street, PO Box, or company name).

        *line2* String

        :   Address line 2 (e.g., apartment, suite, unit, or building).

        *postal\_code* String

        :   ZIP or postal code.

        *state* String

        :   State, county, province, or region.

    *name* String required

    :   Recipient name.

    *carrier* String

    :   The delivery service that shipped a physical product, such as
        Fedex, UPS, USPS, etc.

    *phone* String

    :   Recipient phone (including extension).

    *tracking\_number* String

    :   The tracking number for a physical product, obtained from the
        delivery service. If multiple tracking numbers were generated
        for this purchase, please separate them with commas.

*statement\_descriptor* String

:   For non-card charges, you can use this value as the complete
    description that appears on your customers' statements. Must contain
    at least one letter, maximum 22 characters.

*statement\_descriptor\_suffix* Text

:   Provides information about a card payment that customers see on
    their statements. Concatenated with the prefix (shortened
    descriptor) or statement descriptor that's set on the account to
    form the complete statement descriptor. Maximum 22 characters for
    the concatenated descriptor.

*transfer\_data* Hash

:   The parameters used to automatically create a Transfer when the
    payment succeeds. For more information, see the PaymentIntents use
    case for connected accounts.

    *amount* Integer

    :   The amount that will be transferred automatically when a charge
        succeeds.

*transfer\_group* String

:   A string that identifies the resulting payment as part of a group.
    transfer\_group may only be provided if it has not been set. See the
    PaymentIntents use case for connected accounts for details.

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_intents/create>

### confirm

Provided with a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object or an hash reference, this will confirm the Stripe payment intent
and return a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object.

Possible parameters are:

*error\_on\_requires\_action* Boolean

:   Set to true to fail the payment attempt if the PaymentIntent
    transitions into requires\_action. This parameter is intended for
    simpler integrations that do not handle customer actions, like
    saving cards without authentication.

*mandate* String

:   ID of the mandate to be used for this payment.

*mandate\_data* Hash

:   This hash contains details about the Mandate to create. This
    parameter can only be used with confirm=true.

    *customer\_acceptance* Hash required

    :   This hash contains details about the customer acceptance of the
        Mandate.

        *type* String required

        :   The type of customer acceptance information included with
            the Mandate. One of online or offline.

        *accepted\_at* Datetime

        :   The time at which the customer accepted the Mandate.

        *offline* Hash

        :   If this is a Mandate accepted offline, this hash contains
            details about the offline acceptance.

        *online* Hash

        :   If this is a Mandate accepted online, this hash contains
            details about the online acceptance.

            *ip\_address* String

            :   The IP address from which the Mandate was accepted by
                the customer.

            *user\_agent* String

            :   The user agent of the browser from which the Mandate was
                accepted by the customer.

*off\_session* Boolean

:   Set to true to indicate that the customer is not in your checkout
    flow during this payment attempt, and therefore is unable to
    authenticate. This parameter is intended for scenarios where you
    collect card details and charge them later.

*payment\_method* String

:   ID of the payment method (a PaymentMethod, Card, or compatible
    Source object) to attach to this PaymentIntent.

*payment\_method\_data* Hash

:   If provided, this hash will be used to create a PaymentMethod. The
    new PaymentMethod will appear in the payment\_method property on the
    PaymentIntent.

    *alipay* Hash

    :   If this is an Alipay PaymentMethod, this hash contains details
        about the Alipay payment method.

        No documented property.

    *au\_becs\_debit* Hash

    :   If this is an au\_becs\_debit PaymentMethod, this hash contains
        details about the bank account.

        *account\_number* String

        :   The account number for the bank account.

        *bsb\_number* String

        :   Bank-State-Branch number of the bank account.

    *bacs\_debit* Hash

    :   If this is a bacs\_debit PaymentMethod, this hash contains
        details about the Bacs Direct Debit bank account.

        *account\_number* String

        :   Account number of the bank account that the funds will be
            debited from.

        *sort\_code* String

        :   Sort code of the bank account. (e.g., 10-20-30)

    *bancontact* Hash

    :   If this is a bancontact PaymentMethod, this hash contains
        details about the Bancontact payment method.

        No documented property.

    *billing\_details* Hash

    :   Billing information associated with the PaymentMethod that may
        be used or required by particular types of payment methods.

        *address* Hash

        :   Billing address.

            *city* String

            :   City, district, suburb, town, or village.

            *country* String

            :   Two-letter country code (ISO 3166-1 alpha-2).

            *line1* String

            :   Address line 1 (e.g., street, PO Box, or company name).

            *line2* String

            :   Address line 2 (e.g., apartment, suite, unit, or
                building).

            *postal\_code* String

            :   ZIP or postal code.

            *state* String

            :   State, county, province, or region.

        *email* String

        :   Email address.

        *name* String

        :   Full name.

        *phone* String

        :   Billing phone number (including extension).

    *eps* hash

    :   If this is an eps PaymentMethod, this hash contains details
        about the EPS payment method.

        No documented property.

    *fpx* Hash

    :   If this is an fpx PaymentMethod, this hash contains details
        about the FPX payment method.

        *bank* String required

        :   The customer's bank.

    *giropay* Hash

    :   If this is a giropay PaymentMethod, this hash contains details
        about the Giropay payment method.

        No documented property.

    *grabpay* Hash

    :   If this is a grabpay PaymentMethod, this hash contains details
        about the GrabPay payment method.

        No documented property.

    *ideal* Hash

    :   If this is an ideal PaymentMethod, this hash contains details
        about the iDEAL payment method.

        *bank* String

        :   The customer's bank.

    *interac\_present* Hash

    :   If this is an interac\_present PaymentMethod, this hash contains
        details about the Interac Present payment method.

        No documented property.

    *metadata* Hash

    :   Set of key-value pairs that you can attach to an object. This
        can be useful for storing additional information about the
        object in a structured format. Individual keys can be unset by
        posting an empty value to them. All keys can be unset by posting
        an empty value to metadata.

    *oxxo* Hash

    :   If this is an oxxo PaymentMethod, this hash contains details
        about the OXXO payment method.

        No documented property.

    *p24* Hash

    :   If this is a p24 PaymentMethod, this hash contains details about
        the P24 payment method.

        *bank* String

        :   The customer's bank.

    *sepa\_debit* Hash

    :   If this is a sepa\_debit PaymentMethod, this hash contains
        details about the SEPA debit bank account.

        *iban* String required

        :   IBAN of the bank account.

    *sofort* Hash

    :   If this is a sofort PaymentMethod, this hash contains details
        about the SOFORT payment method.

        *country* String required

        :   Two-letter ISO code representing the country the bank
            account is located in.

    *type* String

    :   The type of the PaymentMethod. An additional hash is included on
        the PaymentMethod with a name matching this value. It contains
        additional information specific to the PaymentMethod type.

*payment\_method\_options* Hash

:   Payment-method-specific configuration for this PaymentIntent.

    *alipay* Hash

    :   If this is a alipay PaymentMethod, this sub-hash contains
        details about the Alipay payment method options.

        No documented property.

    *bancontact* Hash

    :   If this is a bancontact PaymentMethod, this sub-hash contains
        details about the Bancontact payment method options.

        *preferred\_language* String

        :   Preferred language of the Bancontact authorization page that
            the customer is redirected to.

    *card* Hash

    :   Configuration for any card payments attempted on this
        PaymentIntent.

        *cvc\_token*

        :   A single-use cvc\_update Token that represents a card CVC
            value. When provided, the CVC value will be verified during
            the card payment attempt. This parameter can only be
            provided during confirmation.

        *installments*

        :   Installment configuration for payments attempted on this
            PaymentIntent (Mexico Only).

            For more information, see the [installments integration
            guide](https://stripe.com/docs/payments/installments){.perl-module}.

            *enabled* Boolean

            :   Setting to true enables installments for this
                PaymentIntent. This will cause the response to contain a
                list of available installment plans. Setting to false
                will prevent any selected plan from applying to a
                charge.

            *plan* Hash

            :   The selected installment plan to use for this payment
                attempt. This parameter can only be provided during
                confirmation.

                *count* Integer required

                :   For fixed\_count installment plans, this is the
                    number of installment payments your customer will
                    make to their credit card.

                *interval* String required

                :   For fixed\_count installment plans, this is the
                    interval between installment payments your customer
                    will make to their credit card. One of month.

                *type* String required

                :   Type of installment plan, one of fixed\_count.

        *network* String

        :   Selected network to process this PaymentIntent on. Depends
            on the available networks of the card attached to the
            PaymentIntent. Can be only set confirm-time.

        *request\_three\_d\_secure* String

        :   We strongly recommend that you rely on our SCA Engine to
            automatically prompt your customers for authentication based
            on risk level and other requirements. However, if you wish
            to request 3D Secure based on logic from your own fraud
            engine, provide this option. Permitted values include:
            automatic or any. If not provided, defaults to automatic.
            Read our guide on manually requesting 3D Secure for more
            information on how this configuration interacts with Radar
            and our SCA Engine.

    *oxxo* Hash

    :   If this is a oxxo PaymentMethod, this sub-hash contains details
        about the OXXO payment method options.

        *expires\_after\_days* Integer

        :   The number of calendar days before an OXXO voucher expires.
            For example, if you create an OXXO voucher on Monday and you
            set expires\_after\_days to 2, the OXXO invoice will expire
            on Wednesday at 23:59 America/Mexico\_City time.

    *p24* Hash

    :   If this is a p24 PaymentMethod, this sub-hash contains details
        about the Przelewy24 payment method options.

        No property documented.

    *sepa\_debit* Hash

    :   If this is a sepa\_debit PaymentIntent, this sub-hash contains
        details about the SEPA Debit payment method options.

        *mandate\_options* Hash

        :   Additional fields for Mandate creation

            No property provided.

    *sofort* Hash

    :   If this is a sofort PaymentMethod, this sub-hash contains
        details about the SOFORT payment method options.

        *preferred\_language* String

        :   Language shown to the payer on redirect.

*payment\_method\_types* Array of strings

:   The list of payment method types (e.g. card) that this PaymentIntent
    is allowed to use.

*receipt\_email* String

:   Email address that the receipt for the resulting payment will be
    sent to. If receipt\_email is specified for a payment in live mode,
    a receipt will be sent regardless of your email settings.

*return\_url* String

:   The URL to redirect your customer back to after they authenticate or
    cancel their payment on the payment method's app or site. If you'd
    prefer to redirect to a mobile application, you can alternatively
    supply an application URI scheme. This parameter is only used for
    cards and other redirect-based payment methods.

*setup\_future\_usage* String

:   Indicates that you intend to make future payments with this
    PaymentIntent's payment method.

    Providing this parameter will attach the payment method to the
    PaymentIntent's Customer, if present, after the PaymentIntent is
    confirmed and any required actions from the user are complete. If no
    Customer was provided, the payment method can still be attached to a
    Customer after the transaction completes.

    When processing card payments, Stripe also uses setup\_future\_usage
    to dynamically optimize your payment flow and comply with regional
    legislation and network rules, such as SCA.

    If setup\_future\_usage is already set and you are performing a
    request using a publishable key, you may only update the value from
    on\_session to off\_session.

*shipping* Hash

:   Shipping information for this PaymentIntent.

    *address* String required

    :   Shipping address.

        *city* String

        :   City, district, suburb, town, or village.

        *country* String

        :   Two-letter country code (ISO 3166-1 alpha-2).

        *line1* String required

        :   Address line 1 (e.g., street, PO Box, or company name).

        *line2* String

        :   Address line 2 (e.g., apartment, suite, unit, or building).

        *postal\_code* String

        :   ZIP or postal code.

        *state* String

        :   State, county, province, or region.

    *use\_stripe\_sdk* Boolean

    :   Set to true only when using manual confirmation and the iOS or
        Android SDKs to handle additional authentication steps.

    For more information, see Stripe documentation here:
    <https://stripe.com/docs/api/payment_intents/confirm>

### capture

Provided with a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object or an hash reference, this will capture the Stripe payment intent
and return a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object.

Possible parameters are:

*amount\_to\_capture* Integer

:   The amount to capture from the PaymentIntent, which must be less
    than or equal to the original amount. Any additional amount will be
    automatically refunded. Defaults to the full amount\_capturable if
    not provided.

*application\_fee\_amount* Integer

:   The amount of the application fee (if any) that will be requested to
    be applied to the payment and transferred to the application owner's
    Stripe account. The amount of the application fee collected will be
    capped at the total payment amount. For more information, see the
    PaymentIntents use case for connected accounts.

*statement\_descriptor* String

:   For non-card charges, you can use this value as the complete
    description that appears on your customers' statements. Must contain
    at least one letter, maximum 22 characters.

*statement\_descriptor\_suffix* String

:   Provides information about a card payment that customers see on
    their statements. Concatenated with the prefix (shortened
    descriptor) or statement descriptor that's set on the account to
    form the complete statement descriptor. Maximum 22 characters for
    the concatenated descriptor.

*transfer\_data* Hash

:   The parameters used to automatically create a Transfer when the
    payment is captured. For more information, see the PaymentIntents
    use case for connected accounts.

    It has the following properties:

    *amount* Integer

    :   The amount that will be transferred automatically when a charge
        succeeds.

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_intents/capture>

### cancel

Provided with a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object or an hash reference, this will cancel the Stripe payment intent
and return a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object.

Possible parameters are:

*cancellation\_reason* String

:   Reason for canceling this PaymentIntent. Possible values are
    `duplicate`, `fraudulent`, `requested_by_customer`, or `abandoned`

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_intents/cancel>

### list

Provided with an hash reference of parameters, and this will get a list
of payment methods and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Provided with a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object or an hash reference, this will update the Stripe payment intent
and return a
[Net::API::Stripe::Payment::Intent](https://metacpan.org/pod/Net::API::Stripe::Payment::Intent){.perl-module}
object.

Possible parameters are:

*customer* A Stripe customer id

:   Only return PaymentIntents for the customer specified by this
    customer ID.

*created* Hash

:   A filter on the list based on the object created field. The value
    can be a string with an integer Unix timestamp, or it can be a
    dictionary with the following options:

    *gt* Datetime

    :   Return results where the created field is greater than this
        value.

    *gte* Datetime

    :   Return results where the created field is greater than or equal
        to this value.

    *lt* Datetime

    :   Return results where the created field is less than this value.

    *lte* Datetime

    :   Return results where the created field is less than or equal to
        this value.

*ending\_before* String

:   A cursor for use in pagination. ending\_before is an object ID that
    defines your place in the list. For instance, if you make a list
    request and receive 100 objects, starting with obj\_bar, your
    subsequent call can include ending\_before=obj\_bar in order to
    fetch the previous page of the list.

*limit* Integer

:   A limit on the number of objects to be returned. Limit can range
    between 1 and 100, and the default is 10.

*starting\_after* String

:   A cursor for use in pagination. starting\_after is an object ID that
    defines your place in the list. For instance, if you make a list
    request and receive 100 objects, ending with obj\_foo, your
    subsequent call can include starting\_after=obj\_foo in order to
    fetch the next page of the list.

PAYMENT METHODS
---------------

You can **create**, **retrieve**, **update**, **list**, **attach**,
**detach**, payment methods.

### create

Provided with a
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object or an hash reference and this will create a Stripe payment method
and return its
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object.

Possible parameters are:

*type* String. Any of card, fpx, ideal or sepa\_debit

:   

*billing\_details* An hash reference with the following properties: address.city address.country address.line1 address.line2 address.postal\_code address.state email name phone

:   

*metadata* An arbitrary hash reference

:   

*card* An hash reference with the following properties: exp\_month exp\_year number cvc

:   

*fpx* An hash reference with the property *bank*

:   

*ideal* An hash reference with the property *bank*

:   

*sepa\_debit* An hash reference with the property *iban*

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_methods/create>

### retrieve

Provided with a
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object or an hash reference and this will retrieve a Stripe payment
method and return its
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object.

Possible parameters are:

*id* A Stripe payment method id. This is required

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_methods/retrieve>

### update

Provided with a
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object or an hash reference and this will update the Stripe payment
method and return its
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object.

Possible parameters are:

*id* A Stripe payment method id. This is required.

:   

*billing\_details* An hash reference with the following properties: address.city address.country address.line1 address.line2 address.postal\_code address.state email name phone

:   

*metadata* An arbitrary hash reference.

:   

*card* An hash reference with the following properties: exp\_month exp\_year

:   

*sepa\_debit* An hash reference with the following property: iban

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_methods/update>

### list

Provided with an hash reference of parameters, and this will get a list
of payment methods and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*customer* A Stripe customer id

:   

*type* String. One of card fpx ideal or sepa\_debit

:   

*ending\_before* A Stripe payment method id

:   

*limit* Integer

:   

*starting\_after* A Stripe payment method id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_methods/list>

### attach

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
or a
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object or an hash reference and this will attach the Stripe payment
method to the given customer and return its
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object.

Possible parameters are:

*id* A Stripe payment method id

:   

*customer* A Stripe customer id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_methods/attach>

### detach

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
or a
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object or an hash reference and this will dettach the Stripe payment
method from the given customer and return its
[Net::API::Stripe::Payment::Method](https://metacpan.org/pod/Net::API::Stripe::Payment::Method){.perl-module}
object.

Possible parameters are:

*id* A Stripe payment method id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/payment_methods/detach>

PLANS
-----

You can **create**, **retrieve**, **update**, **list**, **delete**
**plans**.

### create

Provided with a
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object or an hash reference of parameters and this will create a Stripe
plan and return its
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object.

Possible parameters are:

*id* A Stripe plan id (optional)

:   

*active* Boolean

:   

*aggregate\_usage* String

:   

*amount* Integer

:   

*amount\_decimal* Decimal

:   

*billing\_scheme* String. One of per\_unit or tiered

:   

*currency* A 3-letter ISO 4217 code such as `jpy` for Japanese Yen or `eur` for Euro

:   

*interval* String. One of day, week, month or year

:   

*interval\_count* Integer

:   

*metadata* An arbitrary hash reference

:   

*nickname* String

:   

*product* A Stripe product id

:   

*tiers* An hash reference with the following properties: up\_to flat\_amount flat\_amount\_decimal unit\_amount unit\_amount\_decimal

:   

*tiers\_mode* String. One of graduated or volume

:   

*transform\_usage* An hash reference with the following properties: divide\_by round

:   

*trial\_period\_days* Integer

:   

*usage\_type* String. One of metered\|licensed

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/plans/create>

### retrieve

Provided with a
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object or an hash reference of parameters and this will retrieve a
Stripe plan and return its
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object.

Possible parameters are:

*id* A Stripe plan id

:   

For more information, see Stripe documentation here:
[hhttps://stripe.com/docs/api/plans/retrieve](hhttps://stripe.com/docs/api/plans/retrieve){.perl-module}

### update

Provided with a
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object or an hash reference of parameters and this will update a Stripe
plan and return its
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object.

Possible parameters are:

*id* A Stripe plan id (optional)

:   

*active* Boolean

:   

*metadata* An arbitrary hash reference

:   

*nickname* String

:   

*product* A Stripe product id

:   

*trial\_period\_days* Integer

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/plans/update>

### list

Provided with an hash reference of parameters, this will get the list of
Stripe plans and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*created* Date or unix timestamp

:   

*email* String. E-mail address

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*product* A Stripe product id

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/plans/list>

### delete

Provided with a
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object or an hash reference of parameters and this will remove a Stripe
plan and return its
[Net::API::Stripe::Billing::Plan](https://metacpan.org/pod/Net::API::Stripe::Billing::Plan){.perl-module}
object.

Possible parameters are:

*id* A Stripe plan id. This is required

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/plans/delete>

PRICES
------

You can **create**, **retrieve**, **update**, **list** products

### create

Provided with a
[Net::API::Stripe::Price](https://metacpan.org/pod/Net::API::Stripe::Price){.perl-module}
object or an hash reference of parameters and this will create a Stripe
product and return its
[Net::API::Stripe::Price](https://metacpan.org/pod/Net::API::Stripe::Price){.perl-module}
object

Possible parameters are:

*active* boolean

:   Whether the price is currently active. Defaults to true.

*billing\_scheme* string

:   

*currency* String

:   Three-letter ISO currency code, in lowercase. Must be a supported
    currency.

*lookup\_key* string

:   

*metadata* Hash

:   Set of key-value pairs that you can attach to an object. This can be
    useful for storing additional information about the object in a
    structured format. Individual keys can be unset by posting an empty
    value to them. All keys can be unset by posting an empty value to
    metadata.

*nickname* String

:   A brief description of the price, hidden from customers.

*product\_data*

:   A hash or
    [Net::API::Stripe::Product](https://metacpan.org/pod/Net::API::Stripe::Product){.perl-module}

*unit\_amount* A number

:   A positive integer in JPY (or 0 for a free price) representing how
    much to charge.

    This is required only if *billing-scheme* is set to `perl-unit`

*recurring*

:   The recurring components of a price such as interval and
    usage\_type.

    Possible properties are :

    *interval* string

    :   

    *aggregate\_usage* string

    :   

    *interval\_count* number

    :   

    *trial\_period\_days* number

    :   

    *usage\_type* string

    :   

*tiers* hash

:   See
    [Net::API::Stripe::Price](https://metacpan.org/pod/Net::API::Stripe::Price){.perl-module}
    for details

*tiers\_mode* string

:   

*transfer\_lookup\_key* string

:   

*transform\_quantity* number

:   

*unit\_amount\_decimal* number

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/prices/create#create_price>

retrieve
--------

Provided with a
[Net::API::Stripe::Price](https://metacpan.org/pod/Net::API::Stripe::Price){.perl-module}
object or an hash reference of parameters and this will retrieve a
Stripe price and return its
[Net::API::Stripe::Price](https://metacpan.org/pod/Net::API::Stripe::Price){.perl-module}
object

Possible parameters are:

*id* A Stripe price id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/prices/retrieve#retrieve_price>

### update

Provided with a
[Net::API::Stripe::Price](https://metacpan.org/pod/Net::API::Stripe::Price){.perl-module}
object or an hash reference of parameters and this will update a Stripe
price and return its updated
[Net::API::Stripe::Price](https://metacpan.org/pod/Net::API::Stripe::Price){.perl-module}
object

As per the Stripe documentation, \"After prices are created, you can
only update their metadata, nickname, and active fields.\" (see
[Products and
Prices](https://stripe.com/docs/billing/prices-guide|){.perl-module})

Possible parameters are:

*id* A Stripe price id

:   

*active* boolean

:   Whether the price is currently active. Defaults to true.

*lookup\_key* string

:   

*metadata* Hash

:   Set of key-value pairs that you can attach to an object. This can be
    useful for storing additional information about the object in a
    structured format. Individual keys can be unset by posting an empty
    value to them. All keys can be unset by posting an empty value to
    metadata.

*nickname* String

:   A brief description of the price, hidden from customers.

*recurring*

:   The recurring components of a price such as interval and
    usage\_type.

    Possible properties are :

    *interval* string

    :   

    *aggregate\_usage* string

    :   

    *interval\_count* number

    :   

    *trial\_period\_days* number

    :   

    *usage\_type* string

    :   

*transfer\_lookup\_key* string

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/prices/update>

### list

Provided with an hash reference of parameters, this will retrieve a list
of Stripe prices and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object

Possible parameters are:

*active* Boolean

:   

*created* Date or unix timestamp

:   

*currency* String

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*lookup\_key* String

:   

*product* [Net::API::String::Product](https://metacpan.org/pod/Net::API::String::Product){.perl-module} id or object

:   

*recurring* Hash with `inerval` and `usage_type` properties

:   

*starting\_after* A Stripe credit note id

:   

*type* String. One of `recurring` or `one_time`

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/prices/list>

PRODUCTS
--------

You can **create**, **retrieve**, **update**, **list**, **delete**
products

### create

Provided with a
[Net::API::Stripe::Product](https://metacpan.org/pod/Net::API::Stripe::Product){.perl-module}
object or an hash reference of parameters and this will create a Stripe
product and return its
[Net::API::Stripe::Product](https://metacpan.org/pod/Net::API::Stripe::Product){.perl-module}
object

Possible parameters are:

*id* An id to be used as a Stripe product id

:   

*active* Boolean

:   

*attributes* An array of up to 5 elements

:   

*caption* String

:   

*deactivate\_on* Date or timestamp

:   

*description* Text

:   

*images* An array of up to 8 images

:   

*metadata* An arbitrary of hash reference

:   

*name* Stripe. Max length of 250 characters

:   

*package\_dimensions* An hash reference with the following properties: height, length, weight and width

:   

*shippable* Boolean

:   

*statement\_descriptor* Text

:   

*type* String. One of good or service

:   

*unit\_label* String

:   

*url* An url. For goods

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/service_products/create>

### retrieve

Provided with a
[Net::API::Stripe::Product](https://metacpan.org/pod/Net::API::Stripe::Product){.perl-module}
object or an hash reference of parameters and this will retrieve a
Stripe product and return its
[Net::API::Stripe::Product](https://metacpan.org/pod/Net::API::Stripe::Product){.perl-module}
object

Possible parameters are:

*id* A Stripe product id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/service_products/retrieve>

### update

Possible parameters are:

*id* A Stripe product id

:   

*active* Boolean

:   

*attributes* An array of up to 5 elements

:   

*caption* String

:   

*deactivate\_on* Date or timestamp

:   

*description* Text

:   

*images* An array of up to 8 images

:   

*metadata* An arbitrary of hash reference

:   

*name* Stripe. Max length of 250 characters

:   

*package\_dimensions* An hash reference with the following properties: height, length, weight and width

:   

*shippable* Boolean

:   

*statement\_descriptor* Text

:   

*type* String. One of good or service

:   

*unit\_label* String

:   

*url* An url. For goods

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/service_products/update>

### list

Provided with an hash reference of parameters, this will retrieve a list
of Stripe products and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object

Possible parameters are:

*active* Boolean

:   

*created* Date or unix timestamp

:   

*email* String. E-mail address

:   

*ending\_before* A Stripe credit note id

:   

*ids* An array

:   

*limit* Integer

:   

*shippable* Boolean

:   

*starting\_after* A Stripe credit note id

:   

*type* String. One of service or good

:   

*url* The product url

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/service_products/list>

### delete

Provided with a
[Net::API::Stripe::Product](https://metacpan.org/pod/Net::API::Stripe::Product){.perl-module}
object or an hash reference of parameters and this will remove a Stripe
product and return its
[Net::API::Stripe::Product](https://metacpan.org/pod/Net::API::Stripe::Product){.perl-module}
object with its property *deleted* set to true.

Possible parameters are:

*id* A Stripe product id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/service_products/delete>

SUBSCRIPTION SCHEDULES
----------------------

You can **create**, **retrieve**, **update**, **list**, **cancel** or
**release** schedules

### create

Provided with a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object or an hash reference of parameters and this will create a Stripe
subscription schedule and return a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object.

Possible parameters are:

*customer* A Stripe customer id

:   

*default\_settings* An hash reference with the following properties:

:   

    billing\_thresholds.amount\_gte

    :   

    billing\_thresholds.reset\_billing\_cycle\_anchor

    :   

    collection\_method

    :   

    default\_payment\_method

    :   

    invoice\_settings.days\_until\_due

    :   

*end\_behavior* String. One of release or cancel

:   

*from\_subscription* Stripe subscription id

:   

*metadata* An aribitrary hash reference

:   

*phases* An array of hash reference with following properties:

:   

    plan

    :   

    price

    :   

    application\_fee\_percent

    :   

    billing\_thresholds

    :   

    collection\_method

    :   

    coupon

    :   

    default\_payment\_method

    :   

    default\_tax\_rates

    :   

    end\_date

    :   

    invoice\_settings

    :   

    iterations

    :   

    tax\_percent

    :   

    trial

    :   

    trial\_end

    :   

*start\_date* Date or timestamp or the word \'now\'

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscription_schedules/create>

### retrieve

Provided with a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object or an hash reference of parameters and this will retrieve a
Stripe subscription schedule and return a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object.

Possible parameters are:

*id* A Stripe subscription schedule id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscription_schedules/retrieve>

### update

Provided with a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object or an hash reference of parameters and this will update a Stripe
subscription schedule and return a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object.

Possible parameters are:

*id* A Stripe subscription id

:   

*default\_settings* An hash reference with the following properties: billing\_thresholds.amount\_gte billing\_thresholds.reset\_billing\_cycle\_anchor collection\_method default\_payment\_method invoice\_settings.days\_until\_due

:   

*end\_behavior* String. One of release or cancel

:   

*from\_subscription* Stripe subscription id

:   

*metadata* An aribitrary hash reference

:   

*phases* An array of hash reference with following properties: plan application\_fee\_percent billing\_thresholds collection\_method coupon default\_payment\_method default\_tax\_rates end\_date invoice\_settings iterations tax\_percent trial trial\_end

:   

*prorate* Boolean

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscription_schedules/update>

### list

Provided with an hash reference of parameters this will get the list of
subscription schedules and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object

Possible parameters are:

*canceled\_at* Unix timestamp

:   

*completed\_at* Unix timestamp

:   

*created* Unix timestamp

:   

*customer* A Stripe customer id

:   

*email* String. E-mail address

:   

*ending\_before* A Stripe subscription schedule id

:   

*limit* Integer

:   

*released\_at* Unix timestamp

:   

*scheduled* Boolean

:   

*starting\_after* A Stripe subscription schedule id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscription_schedules/list>

### cancel

Provided with a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object or an hash reference of parameters and this will cancel a Stripe
subscription schedule and return a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object.

Possible parameters are:

*id* A Stripe subscription schedule. This is required.

:   

*invoice\_now* Boolean

:   

*prorate* Boolean

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscription_schedules/cancel>

### release

Provided with a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object or an hash reference of parameters and this will release a Stripe
subscription schedule and return a
[Net::API::Stripe::Billing::Subscription::Schedule](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription::Schedule){.perl-module}
object.

Possible parameters are:

*id* A Stripe subscription schedule. This is required.

:   

*preserve\_cancel\_date* Boolean

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscription_schedules/release>

CHECKOUT SESSIONS
-----------------

You can **create** or **retrieve** checkout sessions

### **create** {#createcreate}

Provided with a
[Net::API::Stripe::Checkout::Session](https://metacpan.org/pod/Net::API::Stripe::Checkout::Session){.perl-module}
object or an hash reference of parameters and this will create a Stripe
checkout session and return a
[Net::API::Stripe::Checkout::Session](https://metacpan.org/pod/Net::API::Stripe::Checkout::Session){.perl-module}
object.

Possible parameters are:

*cancel\_url* URL

:   

*payment\_method\_types* String. One of card or ideal

:   

*success\_url* URL

:   

*billing\_address\_collection* String. One of auto or required

:   

*client\_reference\_id* String

:   

*customer* A Stripe customer id

:   

*customer\_email* String

:   

*line\_items* An array of hash reference with the following properties: amount currency name quantity description images

:   

*locale* a 2-letter iso 639, such as `fr` or `ja` or `local`

:   

*mode* String. One of setup or subscription

:   

*payment\_intent\_data* An hash reference with the following properties: application\_fee\_amount capture\_method description metadata on\_behalf\_of receipt\_email setup\_future\_usage

:   

*setup\_intent\_data* An hash reference with the following properties: description metadata on\_behalf\_of

:   

*submit\_type* String. One of auto, book, donate or pay

:   

*subscription\_data* An hash reference with the following properties: items application\_fee\_percent metadata trial\_end trial\_from\_plan trial\_period\_days

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/checkout/sessions/create>

### **retrieve** {#retrieveretrieve}

Provided with a
[Net::API::Stripe::Checkout::Session](https://metacpan.org/pod/Net::API::Stripe::Checkout::Session){.perl-module}
object or an hash reference of parameters and this will retrieve a
Stripe checkout session and return a
[Net::API::Stripe::Checkout::Session](https://metacpan.org/pod/Net::API::Stripe::Checkout::Session){.perl-module}
object.

Possible parameters are:

*id* A Stripe checkout session

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/checkout/sessions/retrieve>

### **list** {#listlist}

Provided with an hash reference of parameters, this will get the list of
checkout sessions and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*payment\_intent* A Stripe payment intent id

:   

*subscription* A Stripe subscription id

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/checkout/sessions/list>

SOURCES
-------

You can **create**, **retrieve**, **update**, **detach** or **attach**
sources

### create {#create-14}

Provided with
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object or an hash reference of parameters, this will create a Stripe
source and return a
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object.

Possible parameters are:

*type* String. This is required.

:   

*amount* Integer

:   

*currency* A 3-letter iso 4217 code such as `jpy` or `eur`

:   

*flow* String. One of redirect, receiver, code\_verification, none

:   

*mandate* An hash reference with the following properties: acceptance amount currency interval notification\_method

:   

*metadata* An arbitrary hash reference

:   

*owner* An hash reference with the following properties: address email name phone

:   

*receiver* An hash reference with the following property: refund\_attributes\_method

:   

*redirect* An hash reference with the following property: return\_url

:   

*source\_order* An hash reference with the following properties: items shipping

:   

*statement\_descriptor* Text

:   

*token* String

:   

*usage* String. One of reusable or single\_use

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/sources/create>

### retrieve {#retrieve-16}

Provided with
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object or an hash reference of parameters, this will retrieve a Stripe
source and return a
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object.

Possible parameters are:

*id* A Stripe source id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/sources/retrieve>

### update

Provided with
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object or an hash reference of parameters, this will update a Stripe
source and return a
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object.

Possible parameters are:

*id* A Stripe source id

:   

*amount* Integer

:   

*mandate* An hash reference with the following properties: acceptance amount currency interval notification\_method

:   

*metadata* An arbitrary hash reference

:   

*owner* An hash reference with the following properties: address email name phone

:   

*source\_order* An hash reference with the following properties: items shipping

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/sources/update>

### detach

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object or a
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object or an hash reference of parameters, this will detach a Stripe
source from the customer and return a
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object.

Possible parameters are:

*id* A Stripe customer id

:   

*source* A Stripe source id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/sources/detach>

### attach

Provided with a
[Net::API::Stripe::Customer](https://metacpan.org/pod/Net::API::Stripe::Customer){.perl-module}
object or a
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object or an hash reference of parameters, this will attach a Stripe
source to a customer and return a
[Net::API::Stripe::Payment::Source](https://metacpan.org/pod/Net::API::Stripe::Payment::Source){.perl-module}
object.

Possible parameters are:

*id* A Stripe customer id

:   

*source* A Stripe source id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/sources/attach>

SUBSCRIPTIONS
-------------

You can **create**, **delete\_discount**, **retrieve**, **update**,
**list** or **cancel** subscriptions

### create {#create-15}

Provided with a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object or an hash reference of parameters, this will create a Stripe
subscription and return a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object.

Possible parameters are:

*customer* A Strip customer id. This is required.

:   

*application\_fee\_percent* Decimal

:   

*backdate\_start\_date* Date or timestamp

:   

*billing\_cycle\_anchor* Date or timestamp

:   

*billing\_thresholds* An hash reference with the following properties: amount\_gte reset\_billing\_cycle\_anchor

:   

*cancel\_at* Date or timestamp

:   

*cancel\_at\_period\_end* Boolean

:   

*collection\_method* String. One of charge\_automatically, or send\_invoice

:   

*coupon* String

:   

*days\_until\_due* Integer

:   

*default\_payment\_method* A Stripe payment method id

:   

*default\_source* A Stripe source id

:   

*default\_tax\_rates* Array of string

:   

*items* An array of hash reference with the following properties:

:   

    billing\_thresholds.usage\_gte

    :   

    plan

    :   

    price

    :   

    price\_data.currency

    :   

    price\_data.product

    :   

    price\_data.recurring.interval

    :   

    price\_data.recurring.interval\_count

    :   

    price\_data.unit\_amount\_decimal

    :   

    price\_data.unit\_amount

    :   

    billing\_thresholds

    :   

    metadata

    :   

    quantity

    :   

    tax\_rates

    :   

*metadata* An arbitrary hash reference

:   

*off\_session* Boolean

:   

*payment\_behavior* String. One of allow\_incomplete error\_if\_incomplete or pending\_if\_incomplete

:   

*pending\_invoice\_item\_interval* An hash reference with the following properties: interval interval\_count

:   

*prorate* Boolean

:   

*proration\_behavior* String. One of billing\_cycle\_anchor, create\_prorations or none

:   

*tax\_percent* Decimal

:   

*trial\_end* Unix timestamp or \'now\'

:   

*trial\_from\_plan* Boolean

:   

*trial\_period\_days* Integer

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscriptions/create>

### delete\_discount

Provided with a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object or an hash reference of parameters, this will remove its discount
and return a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object.

Possible parameters are:

*id* A Stripe subscription id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscriptions/create>

### retrieve {#retrieve-17}

Provided with a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object or an hash reference of parameters, this will retrieve a Stripe
subscription and return a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object.

Possible parameters are:

*id* A Stripe subscription id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscriptions/create>

### update

Provided with a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object or an hash reference of parameters, this will update a Stripe
subscription and return a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object.

Possible parameters are:

*id* A Stripe subscription id

:   

*application\_fee\_percent* Decimal

:   

*billing\_cycle\_anchor* Date or timestamp

:   

*billing\_thresholds* An hash reference with the following properties: amount\_gte reset\_billing\_cycle\_anchor

:   

*cancel\_at* Date or timestamp

:   

*cancel\_at\_period\_end* Boolean

:   

*collection\_method* String. One of charge\_automatically, or send\_invoice

:   

*coupon* String

:   

*days\_until\_due* Integer

:   

*default\_payment\_method* A Stripe payment method id

:   

*default\_source* A Stripe source id

:   

*default\_tax\_rates* Array of string

:   

*items* An array of hash reference with the following properties: plan billing\_thresholds metadata quantity tax\_rates

:   

*metadata* An arbitrary hash reference

:   

*off\_session* Boolean

:   

*pause\_collection* An hash reference with the following properties: behavior resumes\_at

:   

*payment\_behavior* String. One of allow\_incomplete error\_if\_incomplete or pending\_if\_incomplete

:   

*pending\_invoice\_item\_interval* An hash reference with the following properties: interval interval\_count

:   

*prorate* Boolean

:   

*prorate\_date* A Date or timestamp

:   

*tax\_percent* Decimal

:   

*trial\_end* Unix timestamp or \'now\'

:   

*trial\_from\_plan* Boolean

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscriptions/create>

### list {#list-16}

Provided with a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object or an hash reference of parameters, this will create a Stripe
subscription and return a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object.

Possible parameters are:

*active* Boolean

:   

*created* Date or unix timestamp

:   

*ids* Array reference

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*shippable* Boolean

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscriptions/create>

### cancel

Provided with a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object or an hash reference of parameters, this will cancel a Stripe
subscription and return a
[Net::API::Stripe::Billing::Subscription](https://metacpan.org/pod/Net::API::Stripe::Billing::Subscription){.perl-module}
object.

Possible parameters are:

*id* A Stripe subscription id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/subscriptions/create>

TAX IDS
-------

You can **create**, **retrieve**, **delete** or **list** tax ids

### create {#create-16}

Provided with a
[Net::API::Stripe::Billing::TaxID](https://metacpan.org/pod/Net::API::Stripe::Billing::TaxID){.perl-module}
object or an hash reference of parameters, this will cancel a Stripe tax
id and return a
[Net::API::Stripe::Billing::TaxID](https://metacpan.org/pod/Net::API::Stripe::Billing::TaxID){.perl-module}
object.

Possible parameters are:

*customer* A Stripe customer id

:   

*type* String, such as au\_abn, ch\_vat, eu\_vat, in\_gst, mx\_rfc, no\_vat, nz\_gst, or za\_vat

:   

*value* String

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customer_tax_ids/create>

### retrieve {#retrieve-18}

Provided with a
[Net::API::Stripe::Billing::TaxID](https://metacpan.org/pod/Net::API::Stripe::Billing::TaxID){.perl-module}
object or an hash reference of parameters, this will cancel a Stripe tax
id and return a
[Net::API::Stripe::Billing::TaxID](https://metacpan.org/pod/Net::API::Stripe::Billing::TaxID){.perl-module}
object.

Possible parameters are:

*id* A Stripe tax id. This is required

:   

*customer* A Stripe customer id. This is required

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customer_tax_ids/retrieve>

### delete

Provided with a
[Net::API::Stripe::Billing::TaxID](https://metacpan.org/pod/Net::API::Stripe::Billing::TaxID){.perl-module}
object or an hash reference of parameters, this will cancel a Stripe tax
id and return a
[Net::API::Stripe::Billing::TaxID](https://metacpan.org/pod/Net::API::Stripe::Billing::TaxID){.perl-module}
object.

Possible parameters are:

*id* A Stripe tax id. This is required

:   

*customer* A Stripe customer id. This is required

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customer_tax_ids/delete>

### list {#list-17}

Provided with an hash reference of parameters, this will get the list of
Stripe tax id and return a
[Net::API::Stripe::List](https://metacpan.org/pod/Net::API::Stripe::List){.perl-module}
object.

Possible parameters are:

*id* A Stripe customer id. This is required

:   

*ending\_before* A Stripe credit note id

:   

*limit* Integer

:   

*starting\_after* A Stripe credit note id

:   

For more information, see Stripe documentation here:
<https://stripe.com/docs/api/customer_tax_ids/list>

ERROR HANDLING
==============

[Net::API::Stripe](https://metacpan.org/pod/Net::API::Stripe){.perl-module}
never dies, or at least not voluntarily. Instead, when an error occurs
and is reported, it returns undef and the error can be retrieved with
the [\"error\"](#error){.perl-module} method, such as:

        my $prod = $stripe->products( retrieve => $prod_id ) || die( $stripe->error, "\n" );

The **error** method returns the
[Module::Generic::Exception](https://metacpan.org/pod/Module::Generic::Exception){.perl-module}
set. Please refer to the manual page of [\"error\" in
Module::Generic](https://metacpan.org/pod/Module::Generic#error){.perl-module}
for more information, but essentially, the following methods are
available with the error objects:

### as\_string

This is triggered when the error object is stringified

### code

The error code returned by Stripe

### file

The file containing the error

### line

The line number of the error

### message

The actual error message

### package

The package name where the error occurred.

### rethrow

Used to re-trigger the error

### subroutine

The subroutine name where the error originates

### trace

The full stack trace object. This is a
[Devel::StackTrace](https://metacpan.org/pod/Devel::StackTrace){.perl-module}

### type

The error type, if any

DATES AND DATETIME
==================

Everywhere Stripe returns a date or datetime,
[Net::API::Stripe](https://metacpan.org/pod/Net::API::Stripe){.perl-module}
will return a
[DateTime](https://metacpan.org/pod/DateTime){.perl-module} object so
one can call the method like this:

        printf( "Object was created on %s\n", $product->created->iso8601 );

NUMBERS
=======

When a number is returned by Stripe,
[Net::API::Stripe](https://metacpan.org/pod/Net::API::Stripe){.perl-module}
will return a
[Module::Generic::Number](https://metacpan.org/pod/Module::Generic::Number){.perl-module},
so one could call the method representing a number like this:

        printf( "Charge was %s\n", $charge->amount->format_money( 2, '€' ) );
        # Assuming the charge amount is 100, this would produce: €100.00

API ACCESS METHODS
==================

All api access methods below from [\"BALANCE
TRANSACTIONS\"](#balance-transactions){.perl-module} and below also take
the *expand* parameter, which can have value \'all\' to expand all
possible objects, or it can have an integer to set the depth of
expanstion or it can be an array reference of object properties to
expand. Sub levels of expansion are noted by a dot between properties.

        my $cust = $stripe->customers( retrieve => { id => $customer_id, expand => [qw( invoice_settings.default_payment_method invoice_settings.default_payment_method.customer )] })

When providing an object as a parameter to an api method, *expand* will
always set automatically to `all`.

BALANCES
--------

You an **retrieve** balances.

### retrieve {#retrieve-19}

        my $bal = $stripe->balances( 'retrieve' ) || die( $stripe->error );

Provided with a
[Net::API::Stripe::Balance](https://metacpan.org/pod/Net::API::Stripe::Balance){.perl-module}
object, or a hash reference, this will retrieve a Stripe balance and
return its
[Net::API::Stripe::Balance](https://metacpan.org/pod/Net::API::Stripe::Balance){.perl-module}
object.

There is no argument.

API SAMPLE
==========

        {
          "object": "balance",
          "available": [
            {
              "amount": 0,
              "currency": "jpy",
              "source_types": {
                "card": 0
              }
            }
          ],
          "connect_reserved": [
            {
              "amount": 0,
              "currency": "jpy"
            }
          ],
          "livemode": false,
          "pending": [
            {
              "amount": 7712,
              "currency": "jpy",
              "source_types": {
                "card": 7712
              }
            }
          ]
        }

HISTORY
=======

<https://stripe.com/docs/upgrades> for Stripe API version history.

AUTHOR
======

Jacques Deguest \<`jack@deguest.jp`{classes="ARRAY(0x556a9a8a59f0)"}\>

SEE ALSO
========

Stripe API documentation:

<https://stripe.com/docs/api>

List of server-side libraries:
<https://stripe.com/docs/libraries#server-side-libraries>

[Net::Stripe](https://metacpan.org/pod/Net::Stripe){.perl-module},
another Stripe API, but which uses Moose

COPYRIGHT & LICENSE
===================

Copyright (c) 2018-2019 DEGUEST Pte. Ltd.

You can use, copy, modify and redistribute this package and associated
files under the same terms as Perl itself.
