##----------------------------------------------------------------------------
## Stripe API - ~/lib/Net/API/Stripe/Payment/Method/Details.pm
## Version v0.1.0
## Copyright(c) 2022 DEGUEST Pte. Ltd.
## Author: Jacques Deguest <jack@deguest.jp>
## Created 2019/11/02
## Modified 2020/11/30
## All rights reserved.
## 
## 
## This program is free software; you can redistribute  it  and/or  modify  it
## under the same terms as Perl itself.
##----------------------------------------------------------------------------
package Net::API::Stripe::Payment::Method::Details;
BEGIN
{
    use strict;
    use warnings;
    use parent qw( Net::API::Stripe::Payment::Method::Options );
    use vars qw( $VERSION );
    our $VERSION = 'v0.1.0';
};

use strict;
use warnings;

1;
# NOTE: POD
__END__

=encoding utf-8

=head1 NAME

Net::API::Stripe::Payment::Method::Details - Stripe API

=head1 SYNOPSIS

    use Net::API::Stripe::Payment::Method::Details;
    my $this = Net::API::Stripe::Payment::Method::Details->new || 
        die( Net::API::Stripe::Payment::Method::Details->error, "\n" );

=head1 VERSION

    v0.1.0

=head1 DESCRIPTION

This package is called in Stripe api, but is really an alias for L<Net::API::Stripe::Payment::Method::Options> and inherits everything from it.

=head1 AUTHOR

Jacques Deguest E<lt>F<jack@deguest.jp>E<gt>

=head1 SEE ALSO

L<perl>

=head1 COPYRIGHT & LICENSE

Copyright(c) 2022 DEGUEST Pte. Ltd.

All rights reserved.

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

=cut
